<?php

/* 
 * Copyright (C) 2014 Ian Craig
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// $Id$
// Assembly-Part Relationship functions for Redback CAD module
// Created by Ian Craig - minicraig@gmail.com

/************************************************************************************************
 * Detect relations in a file
 ************************************************************************************************/
 
function rbc_relation_detect($file) {
	if (!is_object($file) || !file_exists($file->uri)) return;
	//Get this PID
	$matches = array();
	$count = preg_match('/(.+)\.SLDASM/', $file->filename, $matches);
	$this_part_id = $matches[1];
	//Break Down File
	$contents = file_get_contents($file->uri);
	$contents = preg_replace('/[^a-zA-Z \/\\\\\.\-\(\)0-9\n:]/', '', $contents);
	$full = $contents;
	$contents = strstr($contents, 'moAssemblyc');
	$contents = strstr($contents, 'VisualProperties', true);
	//Track changes to parts (get latest change)
	$status = array();
	$deleted = array();
	$matches = array();
	$count = preg_match_all('/(created|modified|deleted)[ \)\/]?(\d{4})-(\d+)/i', $contents, $matches);
	foreach ($matches[2] as $k => $part_id) $status[$part_id][$matches[3][$k]] = $matches[1][$k];
	foreach ($status as $part_id => $instances) {
		$deleted[$part_id] = true;
		foreach ($instances as $i) {
			if ($i != 'Deleted') unset($deleted[$part_id]);
		}
	}
	$matches = array();
	$count = preg_match_all('/' . $path . '(\d{4})\.SLD(PRT|ASM)/i', $contents, $matches);
	$parts = array();
	foreach($matches[1] as $part_id) {
		if (!in_array($part_id, $parts) && $part_id != $this_part_id && !array_key_exists($part_id, $deleted)) $parts[] = $part_id;
	}
	return $parts;
}

/************************************************************************************************
 * Admin Functions
 ************************************************************************************************/

function rbc_relation_detect_test($file) {
	if (!is_object($file) || !file_exists($file->uri)) return;
	//Get this PID
	$matches = array();
	$count = preg_match('/(.+)\.SLDASM/', $file->filename, $matches);
	$this_part_id = $matches[1];
	//Break Down File
	$contents = file_get_contents($file->uri);
	$contents = preg_replace('/[^a-zA-Z \/\\\\\.\-\(\)0-9\n:]/', '', $contents);
	$full = $contents;
	$contents = strstr($contents, 'moAssemblyc');
	$contents = strstr($contents, 'VisualProperties', true);
	//Track changes to parts (get latest change)
	$status = array();
	$deleted = array();
	$matches = array();
	$count = preg_match_all('/(created|modified|deleted)[ \)\/]?(\d{4})-(\d+)/i', $contents, $matches);
	foreach ($matches[2] as $k => $part_id) $status[$part_id][$matches[3][$k]] = $matches[1][$k];
	foreach ($status as $part_id => $instances) {
		$deleted[$part_id] = true;
		foreach ($instances as $i) {
			if ($i != 'Deleted') unset($deleted[$part_id]);
		}
	}
	//Find all numbered parts
	$matches = array();
	$count = preg_match_all('/' . $path . '(\d{4})\.SLD(PRT|ASM)/i', $contents, $matches);
	$parts = array();
	foreach($matches[1] as $part_id) {
		if (!in_array($part_id, $parts) && $part_id != $this_part_id && !array_key_exists($part_id, $deleted)) $parts[] = $part_id;
	}

	dev_print($status);
	dev_print($parts);
	dev_print($contents);
	return 'Ok';
}

function rbc_relation_manual_update($file) {
	if (!is_object($file) || !file_exists($file->uri)) return 'Could not find file';
	//Get this PID
	$matches = array();
	$count = preg_match('/(.+)\.SLDASM/', $file->filename, $matches);
	$this_part_id = $matches[1];
	//Detect Relations
	$relations = rbc_relation_detect($file);
	if (empty($relations)) {
		return 'Assembly does not contain any RBC Parts.';
	} else {
		foreach($relations as $part_id) {
			$part = Part::load($part_id);
			//Check Part Exists
			if (!$part) return 'Assembly containts a part which does not exist.';
		}
	}
	//Add new relations
	rbc_relation_update($this_part_id, $relations);
	
	return 'It is done.';
}

/************************************************************************************************
 * Update relations for an assembly given it's id and an array of it's children
 ************************************************************************************************/
function rbc_relation_update($parent, $children, $year = NULL) {
    if ($year == NULL) $year = $GLOBALS['current_year'];
    db_delete('rbc_relation')
            ->condition('parent_part_id', $parent)
            ->execute();
    foreach($children as $child) {
            db_insert('rbc_relation')
                    ->fields(array(
                            'parent_part_id' => $parent,
                            'child_part_id' => $child,
                    ))
                    ->execute();
    }
}

/************************************************************************************************
 * Get an array of parents or children for a part
 ************************************************************************************************/
 
function rbc_relation_parents($part_id) {
	$parents = db_select('rbc_relation', 'r')
		->fields('r', array('parent_part_id'))
		->condition('child_part_id', $part_id)
                ->condition('year', rbc_year())
		->execute()
		->fetchCol();
	return $parents;
}

function rbc_relation_children($part_id) {
	$children = db_select('rbc_relation', 'r')
		->fields('r', array('child_part_id'))
		->condition('parent_part_id', $part_id)
                ->condition('year', rbc_year())
		->execute()
		->fetchCol();
	return $children;
}