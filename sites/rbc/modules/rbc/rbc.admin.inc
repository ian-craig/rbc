<?php

/* 
 * Copyright (C) 2014 Ian Craig
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// $Id$
// Admin Forms for Redback CAD module
// Created by Ian Craig - minicraig@gmail.com

/* * **********************************************************************************************
 * Cost Script Access
 * ********************************************************************************************** */
function rbc_admin_cost($form, &$form_state) {
    rbc_noreload();
    $form['rbc_cost_key'] = array(
        '#type' => 'textfield',
        '#title' => 'Cost Private Key',
        '#description' => 'Used by excel macros to get access to private data. Leave empty to disable cost pages.',
        '#default_value' => variable_get('rbc_cost_key', NULL),
    );
    $form['submit'] = array(
        '#type' => 'submit',
        '#value' => 'Save',
    );
    return $form;
}

function rbc_admin_cost_submit($form, &$form_state) {
    variable_set('rbc_cost_key', $form_state['values']['rbc_cost_key']);
    drupal_set_message('Settings saved.');
}

/* * **********************************************************************************************
 * File Uploads
 * ********************************************************************************************** */

function rbc_admin_uploads($form, &$form_state) {
    rbc_noreload();
    $form_state['files'] = array(
        'rbc_nopartimage' => array(
            'title' => 'Missing Part Image',
            'description' => 'An image which shows when RBC can\'t generate a thumbnail of the part.',
            'filename' => 'nopartimage',
            'extensions' => 'png jpg jpeg',
        ),
        'rbc_fullplugin' => array(
            'title' => 'Full Plugin Installer',
            'description' => 'The installer for RBC Full Plugin.',
            'filename' => 'RBC Full Plugin Installer',
            'extensions' => 'exe msi zip',
        ),
        'rbc_portableplugin' => array(
            'title' => 'Portable Plugin',
            'description' => 'Standalone executable for RBC Portable Plugin.',
            'filename' => 'RBC Portable Plugin',
            'extensions' => 'exe',
        ),
    );
    foreach ($form_state['files'] as $key => $file) {
        $form[$key] = array(
            '#type' => 'managed_file',
            '#title' => $file['title'],
            '#upload_validators' => array('file_validate_extensions' => array($file['extensions'])),
            '#description' => $file['description'],
            '#default_value' => variable_get($key, NULL),
            '#progress_indicator' => 'bar',
        );
    }
    $form['submit'] = array(
        '#type' => 'submit',
        '#value' => 'Save',
    );
    
    $form['#validate'] = array();
    return $form;
}

function rbc_admin_uploads_submit($form, &$form_state) {
    foreach ($form_state['files'] as $key => $fileinfo) {
        //Delete Old Installer
        $fid = variable_get($key, NULL);
        if ($fid != $form_state['values'][$key]) {
            //Delete old file
            if ($fid != NULL && $file = file_load($fid)) {
                file_usage_delete($file, 'rbc', $key, 1);
                file_delete($file);
            }
            //Save new file
            if ($form_state['values'][$key]) {
               $file = file_load($form_state['values'][$key]);
               $ext = pathinfo($file->uri, PATHINFO_EXTENSION);
               $file->filename = $fileinfo['filename'] . '.' . $ext;
               $file = file_move($file, 'private://rbc/' . $file->filename, FILE_EXISTS_REPLACE);
               $file->status = FILE_STATUS_PERMANENT;
               file_save($file);
               file_usage_add($file, 'rbc', $key, 1);
               variable_set($key, $file->fid);
            }
        }
    }
    drupal_set_message('Settings saved.');
}

/* * **********************************************************************************************
 * Delete Part
 * ********************************************************************************************** */

function rbc_admin_delete_part($form, &$form_state) {
    rbc_noreload();
    //Jump to page 2
    if (isset($form_state['page']) && $form_state['page'] == 'confirm') {
        return rbc_admin_delete_part_confirm($form, $form_state);
    }
    //Add Computer
    $form['part_id'] = array(
        '#type' => 'textfield',
        '#title' => 'Part Number',
        '#size' => 10,
        '#required' => true,
    );
    $form['submit'] = array(
        '#type' => 'submit',
        '#value' => 'Delete',
    );
    return $form;
}

function rbc_admin_delete_part_validate($form, &$form_state) {
    if (!preg_match('/\d{4}/', $form_state['values']['part_id'])) {
        form_set_error('part_id', 'Not a valid Part Number. Part numbers should be 4 digits including leading zeroes.');
    } else {
        $part = Part::load($form_state['values']['part_id']);
        if (!$part->id) {
            form_set_error('part_id', 'This part does not exist.');
        }
    }
}

function rbc_admin_delete_part_submit($form, &$form_state) {
    //Path
    $form_state['part_id'] = $form_state['values']['part_id'];
    //Rebuild
    $form_state['page'] = 'confirm';
    $form_state['rebuild'] = TRUE;
}

function rbc_admin_delete_part_confirm($form, &$form_state) {
    include("rbc.pages.inc");
    $form_state['years'] = db_select('rbc_part_instance', 'i')
            ->fields('i', array('year'))
            ->condition('part_id', $form_state['part_id'])
            ->execute()
            ->fetchCol();
    
    $part = Part::load($form_state['part_id']);
    $form['start_table']['#markup'] = '<h2>Delete ' . $part->name . '</h2><table>';
    foreach ($form_state['years'] as $year) {
        $part = Part::load($form_state['part_id'], $year);
        $image = theme_image_style(array(
            'style_name' => 'thumbnail',
            'path' => rbc_public_uri($part->image()->uri),
            'height' => NULL,
            'width' => NULL,
                ));
        $form[$year] = array(
            '#type' => 'checkbox',
            '#title' => $year,
            '#prefix' => '<tr><td>',
            '#suffix' => '</td><td>' . $image . '</td><td>' . $part->description . '</td></tr>',
        );
    }
    $form['end_table']['#markup'] = '</table>';
    $form['submit'] = array(
        '#type' => 'submit',
        '#value' => 'Confirm Delete',
        '#validate' => array(),
        '#submit' => array('rbc_admin_delete_part_confirm_submit'),
    );
    return $form;
}

function rbc_admin_delete_part_confirm_submit($form, &$form_state) {
    foreach ($form_state['years'] as $year) {
        if (!$form_state['values'][$year]) continue;
         $part = Part::load($form_state['part_id'], $year);
         $part->delete();
         drupal_set_message($part->name . ' (' . $form_state['part_id'] . ') has been deleted for ' . $part->year);
    }
}

/* * **********************************************************************************************
 * Shared Computers
 * ********************************************************************************************** */

function rbc_admin_shared_computers($form, &$form_state) {
    rbc_noreload();
    $computers = db_select('rbc_computer', 'c')
            ->fields('c', array('id'))
            ->condition('uid', '', 'IS NULL')
            ->execute()
            ->fetchCol();

    //Existing Computers
    $build = '<table style="width:auto;"><tr><th>Name</th><th>Status</th><th>Actions</th></tr>';
    foreach ($computers as $computer_id) {
        $computer = Computer::load($computer_id);
        $build .= '<tr><td>' . $computer->name . '</td>';
        $build .= '<td>' . $computer->update_status() . '</td>';
        $build .= '<td><a href="/computers/' . $computer->id . '/delete">Delete</a> <a href="/computers/' . $computer->id . '">Setup</a></td></tr>';
    }
    $build .= '</table>';
    $build .= '<br ><h3>Add New Shared Computer</h3>';
    $form['table']['#markup'] = $build;

    //Add Computer
    $form['name'] = array(
        '#type' => 'textfield',
        '#title' => 'Computer Name',
        '#size' => 20,
        '#required' => true,
    );
    $form['path'] = array(
        '#type' => 'textfield',
        '#title' => 'Folder Path',
        '#description' => '<strong>Please make sure this folder exists.</strong><br />The path where you will place all your cad database files. <br /><em>eg C:\Users\Myname\Documents\RBC\</em><br />',
        '#size' => 70,
        '#required' => true,
    );
    $form['auto_update'] = array(
        '#type' => 'checkbox',
        '#title' => 'Automatically Update',
        '#default_value' => true,
    );
    $form['save'] = array(
        '#type' => 'submit',
        '#value' => 'Add Computer',
    );
    return $form;
}

function rbc_admin_shared_computers_validate($form, &$form_state) {
    $form_state['values']['name'] = check_markup($form_state['values']['name'], 'plain_text');
    if (!preg_match('/^[a-zA-Z]\:\\\\[^\<\>\:\"\/\|\?\*]+$/', $form_state['values']['path']))
            form_set_error('path', 'Folder Path is not valid.');
    if (substr($form_state['values']['path'], -1) != '\\') $form_state['values']['path'] .= '\\';
}

function rbc_admin_shared_computers_submit($form, &$form_state) {
    //Path
    $data['path'] = $form_state['values']['path'];
    //Set all years by default
    $data['years'] = $GLOBALS['years'];
    //Plugin and Auto Update
    $data['plugin'] = true;
    $data['auto_update'] = true;

    $data['standalone'] = false;
    //Save Computer
    $computer_id = db_insert('rbc_computer')
            ->fields(array(
                'name' => $form_state['values']['name'],
                'data' => serialize($data),
            ))
            ->execute();
    drupal_set_message('Added computer <strong>' . $form_state['values']['name'] . '</strong> to shared computers.');
}

/* * **********************************************************************************************
 * Scripts
 * ********************************************************************************************** */

function rbc_admin_scripts($form, &$form_state) {
    $form['refresh_part_images'] = array(
        '#type' => 'submit',
        '#value' => 'Refresh Part Images',
    );
    $form['convert_version_parts'] = array(
        '#type' => 'submit',
        '#value' => 'Update Part File Versioning Method',
    );
    $form['redo_machine_names'] = array(
        '#type' => 'submit',
        '#value' => 'Redo Machine Names',
    );
    $form['convert_commits'] = array(
        '#type' => 'submit',
        '#value' => 'Convert Commits',
    );
    return $form;
}

function rbc_admin_scripts_submit($form, &$form_state) {
    if ($form_state['clicked_button']['#value'] == $form_state['values']['refresh_part_images']) {
        set_time_limit(600);
        $instances = db_select('rbc_part_instance', 'i')
                ->fields('i', array('part_id', 'year'))
                ->execute()
                ->fetchAll();
        $count = 0;
        foreach ($instances as $instance) {
            $part = Part::load($instance->part_id, $instance->year);
            $image = $part->generate_part_image();
             if ($image) {
                $count++;
            } else {
                drupal_set_message('Failed to generate image for ' . $part->name . '(' . $part->id . ') year ' . $part->year, 'error');
            }
        }
        drupal_set_message('Generated part images for ' . $count . ' parts.');
    } else if ($form_state['clicked_button']['#value'] == $form_state['values']['convert_version_parts']) {
        $versions = db_select('rbc_version', 'v')
                ->fields('v')
                ->condition('file_id', '', 'IS NULL')
                ->execute()
                ->fetchAll();
        foreach ($versions as $version) {
            dev_print($version);
            $past = db_select('rbc_version', 'v')
                    ->fields('v')
                    ->condition('part_id', $version->part_id)
                    ->condition('year', $version->year)
                    ->condition('version', $version->version, '<')
                    ->condition('file_id', '', 'IS NOT NULL')
                    ->orderBy('version', 'DESC')
                    ->execute()
                    ->fetchObject();
            $file = file_load($past->file_id);
            file_usage_add($file, 'rbc', 'part', $version->id);
            db_update('rbc_version')
                    ->fields(array('file_id' => $past->file_id))
                    ->condition('id', $version->id)
                    ->execute();
        }
    } else if ($form_state['clicked_button']['#value'] == $form_state['values']['redo_machine_names']) {
        $categories = db_select('rbc_category', 'c')
                ->fields('c')
                ->execute()
                ->fetchAll();
        foreach ($categories as $category) {
            db_update('rbc_category')
                    ->fields(array('machine_name' => rbc_machine_name($category->name)))
                    ->condition('id', $category->id)
                    ->execute();
        }
    } else if ($form_state['clicked_button']['#value'] == $form_state['values']['convert_commits']) {
        rbc_admin_convert_commits();
    }
}

function rbc_admin_convert_commits() {
    $v = db_select('rbc_version', 'v')
            ->fields('v')
            ->orderBy('id', 'ASC')
            ->execute()
            ->fetchAll();
    $len = count($v);
    $i = 0;
    while ($i < $len) {
        $commit = new Commit(false);
        $commit->uid = $v[$i]->uid;
        $commit->message = trim(strip_tags($v[$i]->comment));
        $commit->date = $v[$i]->date;
        $commit->save();
        rbc_admin_convert_commits_setid($v[$i]->id, $commit->id);
        while(++$i < $len) {
            if ($v[$i]->uid == $commit->uid && $v[$i]->year == $v[$i-1]->year) {
                if ($v[$i]->date <= $v[$i-1]->date+300 && strstr($v[$i-1]->comment, $v[$i]->comment)) {
                    // Within 3 minutes and same commit message
                    $commit->date = $v[$i]->date;
                    rbc_admin_convert_commits_setid($v[$i]->id, $commit->id);
                } else if ($v[$i]->date <= $v[$i-1]->date+90) {
                    // Within 1 minute
                    if (substr($commit->message, -1) != '.') $commit->message .= '.';
                    $commit->message .= "\n" . trim(strip_tags($v[$i]->comment));
                    $commit->date = $v[$i]->date;
                    rbc_admin_convert_commits_setid($v[$i]->id, $commit->id);
                } else {
                    break;
                }
            } else {
                break;
            }
        }
        $commit->save();
    }
}

function rbc_admin_convert_commits_setid($vid, $cid) {
    db_update('rbc_version')
            ->fields(array('commit' => $cid))
            ->condition('id', $vid)
            ->execute();
}
