<?php

/* 
 * Copyright (C) 2014 Ian Craig
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

class Commit {
    public $id, $uid, $message, $date;
    private static $fields = array('id', 'uid', 'message', 'date');
    
    public function __construct($load_defaults = true) {
        if ($load_defaults) {
            global $user;
            $this->uid = $user->uid;
            $this->date = time();
        }
    }
    
    public static function load($id = NULL) {
        // Load db info
        $dbentry = db_select('rbc_commit', 'c')
                ->fields('c')
                ->condition('id', $id)
                ->execute()
                ->fetchObject();
        if (!$dbentry) return;
        // Create new object
        $new = new Commit(false);
        foreach ($dbentry as $field => $value)
            $new->$field = $value;
        return $new;
    }
    
    public function save() {
        //TODO Validation
        foreach (self::$fields as $field)
            $db_fields[$field] = $this->$field;
        $this->id = rbc_updateorcreate('rbc_commit', $db_fields);
    }
    
    public function path() {
        return 'commit/' . $this->id;
    }
    
    public function teaser() {
        $cut = false;
        $rows = explode("\n", $this->message);
        if (count($rows) > 1) $cut = true;
        $s0 = trim($rows[0]);
        if (strlen($s0) > 60) {
            $cut = true;
            $s1 = substr($s0, 0, 57);
            $string = substr($s1,0,strrpos($s1, " ", -1));
        } else {
            $string = $s0;
        }
        if ($cut) $string .= " ...";
        return check_markup(l('#'.$this->id, $this->path()).': '.$string, 'filtered_html');
    }
}
