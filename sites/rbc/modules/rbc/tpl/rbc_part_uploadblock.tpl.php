<?php 

/* 
 * Copyright (C) 2014 Ian Craig
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

?>

<div class="uploadblock <?php if ($disabled) print 'disabled'; ?>" part_id="<?php print $part->part_id_str; ?>">
    <?php print theme_image_style(array(
            'style_name' => 'part_table_thumb',
            'path' => rbc_public_uri($part->image()->uri),
            'height' => NULL,
            'width' => NULL,
            'attributes' => array('style' => 'float:left;'),
    )); ?>
    <div class="info">
        <?php print $part->name; ?>
        <br />
        <?php print $part->part_id_str; ?>
    </div>
    <div class="state-wrapper">
        <div class="state <?php print $state; ?>">
            <div class="statebg"></div>
            <div class="text"><?php print $state_text; ?></div>
        </div>
    </div>
    <div class="close-box">x</div>
</div>