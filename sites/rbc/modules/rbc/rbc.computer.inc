<?php

/* 
 * Copyright (C) 2014 Ian Craig
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

class Computer {

    public $id, $uid, $name, $data; // Cols
    private static $col_fields = array('id', 'uid', 'name'); // Array of standard col names
    public $version, $auto_update, $paused, $years; // Data
    private static $data_fields = array('version', 'auto_update', 'years', 'paused'); // Array of data names
    private $updates = array();

    const PROTOCOL = "http://127.0.0.1:6363/";

    public function __construct() {
        // Set defaults
        $this->version = 0;
        $this->auto_update = false;
        $this->years = array($GLOBALS['current_year']);
        $this->paused = false;
    }

    public static function load($computer_id = NULL) {
        // Load computer ID from session info
        if ($computer_id == NULL && isset($_SESSION['computer_id']))
                $computer_id = $_SESSION['computer_id'];
        if (!$computer_id) return;
        // Load computer info
        $dbentry = db_select('rbc_computer', 'c')
                ->fields('c', array('id', 'uid', 'name', 'data'))
                ->condition('id', $computer_id)
                ->execute()
                ->fetchObject();
        if (!$dbentry) return;
        $computer = new Computer();
        foreach ($dbentry as $field => $value)
            $computer->$field = $value;
        $computer->data = unserialize($computer->data);
        // Expand data
        if (isset($computer->data['version']))
                $computer->version = $computer->data['version'];
        if (isset($computer->data['auto_update']))
                $computer->auto_update = $computer->data['auto_update'];
        if (isset($computer->data['years']))
                $computer->years = $computer->data['years'];
        if (isset($computer->data['paused']))
                $computer->paused = $computer->data['paused'];
        return $computer;
    }

    public function save() {
        //TODO Validation
        // Save data
        foreach (self::$data_fields as $field)
            $this->data[$field] = $this->$field;
        // Compile cols
        $db_fields = array();
        foreach (self::$col_fields as $field)
            $db_fields[$field] = $this->$field;
        $db_fields['data'] = serialize($this->data);
        $this->id = rbc_updateorcreate('rbc_computer', $db_fields);
    }

    public function plugin_update_available() {
        return $this->version < $GLOBALS['plugin_version'];
    }

    function file_count() {
        $num_files = db_select('rbc_download', 'd')
                ->fields('d')
                ->condition('computer_id', $this->id)
                ->execute()
                ->rowCount();
        return $num_files;
    }

    public function updates($time = NULL) {
        if ($time) {
            if (isset($this->updates[$time])) return $this->updates[$time];
        } else {
            $latest = $this->updates ? max(array_keys($this->updates)) : 0;
            if ($latest > time() - 5) return $this->updates[$latest];
            $time = time() + 1;
        }
        $updates[$time] = array();
        // Fetch a list of all the latest versions of active parts for the years we want
        $ids = db_query('SELECT DISTINCT MAX(id) FROM rbc_version WHERE year IN (' . implode(',', $this->years) . ') and (SELECT `removed` FROM `rbc_part_instance` WHERE part_id=rbc_version.part_id and year=rbc_version.year)=0 GROUP BY part_id, year')->fetchCol();
        if ($ids) {
            // Fetch info about each of these relating to our download history
            $select = db_select('rbc_version', 'v');
            $select->leftJoin('rbc_download', 'd', 'd.part_id=v.part_id AND d.year=v.year AND d.computer_id=' . $this->id);
            $select->addField('v', 'part_id');
            $select->addField('v', 'year');
            $select->addField('d', 'date', 'download_date');
            $select->addField('v', 'date', 'version_date');
            $select->condition('v.id', $ids, 'IN');
            $rows = $select->execute()->fetchAll();
            foreach ($rows as $row) {
                if ($row->download_date == NULL || $row->download_date < $row->version_date) {
                    $updates[$time][$row->year][] = $row->part_id;
                }
            }
        }
        return $updates[$time];
    }

    function update_count($time = NULL) {
        $updates = $this->updates($time);
        $count = 0;
        foreach ($updates as $year_updates)
            $count += count($year_updates);
        return $count;
    }

    function update_status($time = NULL) {
        $count = $this->update_count($time);
        return ($count) ? $count . ' Updates Available' : 'Up to Date';
    }

    function download_date($part_id, $year = NULL) {
        return rbc_download_date($part_id, $year, $this->id);
    }

    function clear_downloads($parts = NULL) {
        if ($parts) {
            $or = db_or();
            foreach ($parts as $year => $part_ids) {
                $or->condition(db_and()->condition('year', $year)->condition('part_id', $part_ids, 'IN'));
            }
        }
        $q = db_delete('rbc_download')
                ->condition('computer_id', $this->id);
        if ($parts) $q->condition($or);
        $q->execute();
    }
    
    function render_updatestable() {
        $header = array(
            array('data' => 'Part Name'),
            array('data' => 'Year'),
            array('data' => 'Date'),
            array('data' => 'Comment'),
            array('data' => 'User'),
        );
        $rows = array();
        foreach ($this->updates() as $year => $part_ids) {
            foreach ($part_ids as $part_id) {
                $select = db_select('rbc_version', 'v');
                $select->join('rbc_part', 'p', 'p.id=v.part_id');
                $select->join('rbc_commit', 'c', 'c.id=v.commit');
                $select->addField('p', 'name');
                $select->addField('v', 'part_id');
                $select->addField('v', 'date');
                $select->addField('v', 'version');
                $select->addField('c', 'message');
                $select->addField('v', 'commit');
                $select->addField('v', 'uid');
                $select->condition('v.part_id', $part_id);
                $select->condition('v.year', $year);
                $select->orderBy('v.version', 'DESC');
                $update = $select->execute()->fetchObject();
                $rows[$update->date . '_' . $update->part_id] = array(
                    'data' => array(
                        pl($update->part_id),
                        $year,
                        format_date($update->date),
                        l('#' . $update->commit, 'commit/' . $update->commit) . ': ' . $update->message,
                        ul($update->uid),
                    )
                );
                krsort($rows);
            }
        }
        return theme('table', array(
            'header' => $header,
            'rows' => $rows,
            'empty' => 'Up To Date',
        ));
    }

}
