Redback CAD - A simplified web-based version control system for 3D CAD files. Designed primarily to work with SolidWorks files for a Formula SAE / Formula Student team.

The system was developed in an effort to bring the benefits of version control systems like Subversion to a large set of 3D models and assemblies while making the system simple enough for new users with no technical or computing background.

This project is comprised of a Drupal module and theme. Further setup required may include database tables and folder structures. The aim is to add this information to the project when I have time but if you need help with setup before this time please email ian-craig@outlook.com

A Windows PC plugin is available to complement the website (https://bitbucket.org/ian-craig/rbc-plugin). The plugin manages a checked out version of the files; downloading, uploading and opening. It is required for most functionality.