<?php

/* 
 * Copyright (C) 2014 Ian Craig
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// $Id$
// Search functions for Redback CAD module
// Created by Ian Craig - minicraig@gmail.com

function rbc_search_info() {
    return array(
        'title' => 'CAD Database',
    );
}

function rbc_search_access() {
    return user_access('download private files');
}

function rbc_search_execute($keys = NULL, $conditions = NULL) {
    //Fix for title not always working
    drupal_set_title('Search ' . rbc_year() . ' CAD Database');
    //Fix for searches going to the wrong url
    if (rbc_year() != $GLOBALS['current_year'] && substr(current_path(), 0, 5) != rbc_year_prefix()) {
        drupal_goto(rbc_year_prefix() . current_path());
    }
    if (rbc_part_id_load($keys)) { //If the search term exactly matches the part_id pattern
        $part = Part::load($keys, rbc_year());
        if ($part && $part->version != NULL) drupal_goto($part->path());
    }
    $results = array();
    $used_part_ids = array();
    $remaining = 15;
    foreach (array('p.id', 'p.name', 'i.description') as $col) {
        // Replace wildcards with MySQL/PostgreSQL wildcards.
        $keys = preg_replace('!\*+!', '%', $keys);
        $query = db_select('rbc_part', 'p')->extend('PagerDefault');
        $query->join('rbc_part_instance', 'i', 'p.id=i.part_id');
        $query->fields('p', array('id'));
        $query->orderBy('is_assembly', 'DESC');
        $query->condition($col, '%' . db_like($keys) . '%', 'LIKE');
        $query->condition('i.year', rbc_year());
        if ($used_part_ids) $query->condition('p.id', $used_part_ids, 'NOT IN');
        $part_ids = $query->distinct()
                ->limit($remaining)
                ->execute()
                ->fetchCol();
        foreach ($part_ids as $part_id) {
            $used_part_ids[] = $part_id;
            $remaining--;
            $part = Part::load($part_id);
            $user_field = l($GLOBALS['departments'][rbc_year()][$part->department], $part->department);
            if ($part->category_id) {
                $cat_name = db_select('rbc_category', 'c')
                        ->fields('c', array('name'))
                        ->condition('id', $part->category_id)
                        ->execute()
                        ->fetchField();
                $user_field .= " - " . l($cat_name, preg_replace('/\/[^\/]+$/', '', $part->path()));
            }
            $results[] = array(
                'title' => $part->name,
                'link' => url($part->path(), array('absolute' => TRUE)),
                'snippet' => $part->description,
                'date' => $part->date,
                'user' => $user_field,
                'extra' => array('Version ' . $part->version),
            );
        }
    }

    return $results;
}