<?php

/* 
 * Copyright (C) 2014 Ian Craig
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// $Id$
// Automatic Update functions for Redback CAD module
// Created by Ian Craig - minicraig@gmail.com

/* * **********************************************************************************************
 * Updater Requests
 * ********************************************************************************************** */

//Request Number of Updates Available
function rbc_plugin_js_count() {
    $computer = Computer::load();
    if (!$computer) return;
    print $computer->update_count();
}

//Request Download
function rbc_plugin_js_update() {
    $computer = Computer::load();
    if (!$computer) return;
    $data = array('updates' => $computer->updates());
    if (!$data['updates']) return;
    $token = rbc_plugin_new_token();
    db_insert('rbc_plugin_active')
            ->fields(array(
                'computer_id' => $computer->id,
                'token' => $token,
                'action' => 'update',
                'date' => time(),
                'status' => 0,
                'total' => $computer->update_count(),
                'data' => serialize($data),
            ))
            ->execute();
    print rbc_plugin_uri('update', array('key' => $token, 'year' => rbc_year()));

    //Update computer to unpause auto
    $computer->paused = false;
    $computer->save();
}

//Request Progress
function rbc_plugin_js_stat() {
    $computer = Computer::load();
    if (!$computer) return;
    $active = rbc_load_plugin_active(array('computer_id' => $computer->id));
    if (!$active || $active->action != 'update') return;
    $response = array(
        'active' => ($active->token) ? 'true' : '',
        'status' => (int)$active->status,
        'total' => (int)$active->total,
    );
    print json_encode($response);
}

//Plugin file list request
function rbc_plugin_get_list($active) {
    $updates = $active->data['updates'];
    foreach ($updates as $year => $files) {
        foreach ($files as $k => $part_id) {
            $file = rbc_part_file($part_id, $year);
            $updates[$year][$k] = $file->filename;
        }
    }
    print json_encode($updates);
}

//Download File
function rbc_plugin_download($active, $year, $filename) {
    //Check file matches formatting
    $matches = array();
    if (!preg_match('/^(\d{4})(-\d{4})?\.\w+$/', $filename, $matches))
            return MENU_NOT_FOUND;
    //Mark previous download as success
    rbc_plugin_download_success($active);
    //Identify File
    $part_id = intval($matches[1]);
    //Check file is to be downloaded
    if (!isset($active->data['updates'][$year]) || !in_array($part_id, $active->data['updates'][$year])) {
        dev_print($year);
        dev_print($active->data['updates']);
        return MENU_NOT_FOUND;
    }

    //Load File
    $file = rbc_part_file($part_id, $year);
    //Check file extension matches etc
    if ($file->filename != $filename) return MENU_NOT_FOUND;

    //Download confirmed - update active status
    db_update('rbc_plugin_active')
            ->fields(array(
                'part_id' => $part_id,
                'year' => $year,
                'date' => time(),
            ))
            ->condition('id', $active->id)
            ->execute();
    //Send File
    set_time_limit(180);
    header('Content-Disposition: attachment; filename="' . $file->filename . '"');
    print file_get_contents($file->uri);
}

//Skip File
function rbc_plugin_skip($active, $year, $filename) {
    //Check file matches formatting
    $matches = array();
    if (!preg_match('/^(\d{4})(-\d{4})?\.\w+$/', $filename, $matches))
            return MENU_NOT_FOUND;
    //Mark previous download as success
    rbc_plugin_download_success($active);
    //Identify File
    $part_id = intval($matches[1]);
    //Check file is to be downloaded
    if (!isset($active->data['updates'][$year]) || !in_array($part_id, $active->data['updates'][$year]))
            return MENU_NOT_FOUND;
    //Load File
    $file = rbc_part_file($part_id);
    //Check file extension matches etc
    if ($file->filename != $filename) return MENU_NOT_FOUND;
    //Download confirmed - update active status
    db_update('rbc_plugin_active')
            ->fields(array(
                'part_id' => -1,
                'date' => time(),
                'status' => $active->status + 1,
            ))
            ->condition('id', $active->id)
            ->execute();
    //Update computer so we don't update loop
    $computer = Computer::load($active->computer_id);
    $computer->paused = true;
    $computer->save();
}

//Final download completed
function rbc_plugin_update($active) {
    // Mark previous download as success
    rbc_plugin_download_success($active);
    $data = array(
        'token' => '',
        'date' => time(),
        'status' => 1,
    );
    db_update('rbc_plugin_active')
            ->fields($data)
            ->condition('id', $active->id)
            ->execute();
}

//Mark previous download as success
function rbc_plugin_download_success($active) {
    // Check we are active
    if (!$active->date || !$active->computer_id) return;
    
    // Increment Update
    $updated = db_update('rbc_plugin_active')
            ->fields(array(
                'status' => $active->status + 1,
                'part_id' => 0,
            ))
            ->condition('id', $active->id)
            ->execute();
    
    // Look for success GET
    if (!isset($_GET['success'])) return;
    $frag = explode('|', $_GET['success']);
    if (count($frag) != 2) return;
    
    // Check the file was to be downloaded
    $matches = array();
    if (!preg_match('/^(\d{4})(-\d{4})?\.\w+$/', $frag[1], $matches)) return;
    $part_id = intval($matches[1]);
    $year = intval($frag[0]);
    if (!isset($active->data['updates'][$year]) || !in_array($part_id, $active->data['updates'][$year]))
            return;
    
    // Set file downloaded
    $updated = db_update('rbc_download')
            ->fields(array('date' => $active->date))
            ->condition('computer_id', $active->computer_id)
            ->condition('part_id', $part_id)
            ->condition('year', $year)
            ->execute();
    if (!$updated) {
        db_insert('rbc_download')
                ->fields(array(
                    'computer_id' => $active->computer_id,
                    'date' => $active->date,
                    'part_id' => $part_id,
                    'year' => $year,
                ))
                ->execute();
    }
}

/* * **********************************************************************************************
 * Version Request
 * ********************************************************************************************** */

//Request Version Check Link
function rbc_plugin_js_version_request() {
    echo rbc_plugin_version_request();
}

//Request Version Check Progress
function rbc_plugin_js_version_stat() {
    $computer = Computer::load();
    if (!$computer) return;
    $active = rbc_load_plugin_active(array('computer_id' => $computer->id));
    if (!$active || $active->action != 'version') return;
    if ($active->token) {
        echo 'waiting';
    } else {
        if ($active->status == -2) {
            echo 'timeout';
        } else if ($active->status == 1) {
            if ($computer->plugin_update_available()) {
                echo 'out-of-date';
            } else {
                echo 'up-to-date';
            }
        }
    }
}

//Get request url for version request
function rbc_plugin_version_request() {
    $computer = Computer::load();
    if (!$computer) return;

    $token = rbc_plugin_new_token();
    db_insert('rbc_plugin_active')
            ->fields(array(
                'computer_id' => $computer->id,
                'token' => $token,
                'action' => 'version',
                'date' => time(),
                'status' => 0,
            ))
            ->execute();
    return rbc_plugin_uri('version', array('key' => $token));
}

//Recieve version callback
function rbc_plugin_version($active) {
    if (!$_GET['version'] || !is_numeric($_GET['version']))
            return MENU_NOT_FOUND;
    $computer = Computer::load($active->computer_id);
    if (!$computer) return MENU_NOT_FOUND;

    //Mark active finished
    db_update('rbc_plugin_active')
            ->fields(array(
                'token' => '',
                'date' => time(),
                'status' => 1,
            ))
            ->condition('id', $active->id)
            ->execute();
    //Update computer database
    $computer->version = intval($_GET['version']);
    $computer->save();
    drupal_set_message($computer->data['version']);
}

/* * **********************************************************************************************
 * Path Check Request
 * ********************************************************************************************** */

//Request Path Check
function rbc_plugin_js_setup_request() {
    $computer = Computer::load();
    if (!$computer) return;

    $token = rbc_plugin_new_token();
    db_insert('rbc_plugin_active')
            ->fields(array(
                'computer_id' => $computer->id,
                'token' => $token,
                'action' => 'setup',
                'date' => time(),
                'status' => 0,
            ))
            ->execute();
    print rbc_plugin_uri('setup', array('key' => $token, 'id' => $computer->id));
}

//Request Check Path Progress
function rbc_plugin_js_setup_stat() {
    $computer = Computer::load();
    if (!$computer) return;
    $active = rbc_load_plugin_active(array('computer_id' => $computer->id));
    if (!$active || $active->action != 'setup') return;
    $response = array(
        'active' => ($active->token) ? 'true' : '',
        'status' => (int)$active->status,
        'version' => ($computer->plugin_update_available()) ? 0 : 1,
    );
    print json_encode($response);
}

//Recieve path callback
function rbc_plugin_setup($active) {
    if (!$_GET['version'] || !is_numeric($_GET['version']))
            return MENU_NOT_FOUND;
    $computer = Computer::load($active->computer_id);
    if (!$computer) return MENU_NOT_FOUND;
    //Mark active finished
    db_update('rbc_plugin_active')
            ->fields(array(
                'token' => '',
                'date' => time(),
                'status' => 1,
            ))
            ->condition('id', $active->id)
            ->execute();
    //Update computer database
    $computer->version = intval($_GET['version']);
    $computer->save();
}

function rbc_plugin_upload($active) {
    if ($active->action != 'upload') {
        print "UNEXPECTED REQUEST";
        return;
    }

    if (isset($_POST['file_not_found'])) {
        db_update('rbc_plugin_active')
                ->fields(array(
                    'token' => '',
                    'date' => time(),
                    'status' => -3,
                ))
                ->condition('id', $active->id)
                ->execute();
        print "SUCCESS";
        return;
    }

    $contents = isset($_POST['file']) ? $_POST['file'] : NULL;

    if (!$contents) {
        db_update('rbc_plugin_active')
                ->fields(array(
                    'token' => '',
                    'date' => time(),
                    'status' => -1,
                ))
                ->condition('id', $active->id)
                ->execute();
        print "INVALID FILE";
        return;
    }

    $file = file_save_data($contents, 'private://rbc/plugin_upload/' . $active->id, FILE_EXISTS_REPLACE);
    if ($file) {
        $file->filename = $active->data['filename'];
        $file->status = 0; //Temporary
        file_save($file);
    }
    $status = $file ? 2 : -1;
    //Mark active finished
    db_update('rbc_plugin_active')
            ->fields(array(
                'token' => '',
                'date' => time(),
                'status' => $status,
                'data' => serialize($file),
            ))
            ->condition('id', $active->id)
            ->execute();
    print "SUCCESS";
}

function rbc_plugin_js_upload_request() {
    $computer = Computer::load();
    if (!$computer) return;

    if (!isset($_GET['part_id']) || !isset($_GET['year'])) return;
    $part = Part::load($_GET['part_id'], $_GET['year']);
    if (!$part->version_id) return; //Only support existing part instances

    $token = rbc_plugin_new_token();
    $id = db_insert('rbc_plugin_active')
            ->fields(array(
                'computer_id' => $computer->id,
                'token' => $token,
                'action' => 'upload',
                'date' => time(),
                'status' => 0,
                'part_id' => $part->id,
                'year' => $part->year,
                'data' => serialize(array('filename' => $part->file()->filename)),
            ))
            ->execute();
    $response = array(
        'url' => rbc_plugin_uri('upload', array('key' => $token, 'year' => $part->year, 'file' => $part->file()->filename)),
        'id' => $id,
    );
    print json_encode($response);
}

function rbc_plugin_js_upload_stat() {
    $computer = Computer::load();
    $part_id = isset($_GET['part_id']) ? intval($_GET['part_id']) : NULL;
    if (!$part_id || !$computer) return;
    $active = rbc_load_plugin_active(array('computer_id' => $computer->id, 'part_id' => $part_id));
    if (!$active || $active->action != 'upload') return;

    $response = array(
        'status' => $active->status,
        'part_id' => format_part_id($active->part_id),
    );
    if ($active->status == 2) {
        $response['part_fid'] = $active->data->fid;
    }
    print json_encode($response);
}

/* * **********************************************************************************************
 * Other
 * ********************************************************************************************** */

function rbc_plugin_identity($active) {
    if (!isset($_GET['id']) || !is_numeric($_GET['id']) 
            || !isset($_GET['version']) || !is_numeric($_GET['version']))
            return MENU_NOT_FOUND;
    if ($_GET['id'] == 0) {
        $id = 0;
    } else {
        $computer = Computer::load($_GET['id']);
        if (!$computer || $computer->uid && $computer->uid != $active->total) {
            db_update('rbc_plugin_active')
                ->fields(array(
                    'token' => '',
                    'date' => time(),
                    'status' => -3,
                ))
                ->condition('id', $active->id)
                ->execute();
            return MENU_NOT_FOUND;
        }
        $id = $computer->id;
    }
    //Mark active finished
    db_update('rbc_plugin_active')
            ->fields(array(
                'computer_id' => $id,
                'token' => '',
                'date' => time(),
                'status' => 1,
            ))
            ->condition('id', $active->id)
            ->execute();
    //Update computer database
    if ($id && $computer->version != intval($_GET['version'])) {
        $computer->version = intval($_GET['version']);
        $computer->save();
    }
}

function rbc_plugin_identity_request() {
    global $user;
    $key = rbc_plugin_new_token();
    $_SESSION['identity_active'] = db_insert('rbc_plugin_active')
            ->fields(array(
                'computer_id' => 0,
                'token' => $key,
                'action' => 'identity',
                'date' => time(),
                'status' => 0,
                'total' => $user->uid,
            ))
            ->execute();
    return rbc_plugin_uri('identity', array('key' => $key));
}

function rbc_plugin_js_identity_stat() {
    $result = array('status' => -5);
    if (isset($_SESSION['identity_active'])) {
        $active = rbc_load_plugin_active(array(
            'id' => $_SESSION['identity_active'],
            'action' => 'identity',
        ));
        if ($active) {
            $result['status'] = (int)$active->status;
            if ($active->status > 0) {
                if ($active->computer_id) {
                    $computer = Computer::load($active->computer_id);
                    $result['name'] = $computer->name;
                    $_SESSION['computer_id'] = $computer->id;
                    unset($_SESSION['identity_active']);
                } else {
                    $result['name'] = 'Unknown PC';
                    $result['status'] = 2;
                }
            } else if ($active->status < 0) {
                if ($active->status == -2) $result['status'] = $active->status;
                unset($_SESSION['identity_active']);
            }
        }
    }
    print json_encode($result);
}

//Generate a random token that is not currently being used
function rbc_plugin_new_token() {
    $length = 32;
    $possible = "1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    $possible_len = strlen($possible);
    $token = '';

    while (!$token) {
        //Generate a token
        for ($i = 0; $i < $length; $i++) {
            $token .= substr($possible, mt_rand(0, $possible_len - 1), 1);
        }
        //Check token is not in database
        $exists = db_select('rbc_plugin_active', 'a')
                ->fields('a', array('id'))
                ->condition('token', $token)
                ->execute()
                ->fetchField();
        if ($exists) $token = '';
    }
    return $token;
}
