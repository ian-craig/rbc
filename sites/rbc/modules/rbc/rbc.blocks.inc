<?php

/* 
 * Copyright (C) 2014 Ian Craig
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// $Id$
// Block Functions for Redback CAD module
// Created by Ian Craig - minicraig@gmail.com

/**
 * Implements hook_block_info().
 */
function rbc_block_info() {
    $blocks['rbc_navi_menu'] = array(
        'info' => t('RBC Navigation Menu'),
        'status' => TRUE,
        'weight' => -100,
        'visibility' => BLOCK_VISIBILITY_NOTLISTED,
        'pages' => '',
        'region' => 'navigation',
        'cache' => DRUPAL_CACHE_PER_PAGE,
    );
    $blocks['rbc_computers'] = array(
        'info' => t('My Computers'),
        'status' => TRUE,
        'weight' => -100,
        'visibility' => BLOCK_VISIBILITY_NOTLISTED,
        'pages' => '', //rbc_block_visibility_code('computers'),
        'region' => 'sidebar_first',
        'cache' => DRUPAL_NO_CACHE,
    );
    $blocks['rbc_year_switcher'] = array(
        'info' => t('Year Switcher'),
        'status' => TRUE,
        'weight' => -90,
        'visibility' => BLOCK_VISIBILITY_NOTLISTED,
        'pages' => '',
        'region' => 'sidebar_first',
        'cache' => DRUPAL_NO_CACHE,
    );
    $blocks['rbc_action_buttons'] = array(
        'info' => t('Action Buttons'),
        'status' => TRUE,
        'weight' => -100,
        'visibility' => BLOCK_VISIBILITY_NOTLISTED,
        'pages' => '',
        'region' => 'highlighted',
        'cache' => DRUPAL_NO_CACHE,
    );
    $blocks['rbc_part_management'] = array(
        'info' => t('Part Management'),
        'status' => TRUE,
        'weight' => 0,
        'visibility' => BLOCK_VISIBILITY_NOTLISTED,
        'pages' => '',
        'region' => 'sidebar_first',
        'cache' => DRUPAL_NO_CACHE,
    );
    $blocks['rbc_reserved_parts'] = array(
        'info' => t('Reserved'),
        'status' => TRUE,
        'weight' => 10,
        'visibility' => BLOCK_VISIBILITY_NOTLISTED,
        'pages' => '',
        'region' => 'sidebar_first',
        'cache' => DRUPAL_NO_CACHE,
    );
    $blocks['rbc_breadcrumb'] = array(
        'info' => t('Breadcrumb'),
        'status' => TRUE,
        'weight' => -10,
        'visibility' => BLOCK_VISIBILITY_NOTLISTED,
        'pages' => '',
        'region' => 'highlighted',
        'cache' => DRUPAL_NO_CACHE,
    );
    return $blocks;
}

/**
 * Implement hook_block_view().
 */
function rbc_block_view($delta = '0') {
    //Get user id
    global $user;
    $block = '';

    switch ($delta) {
        //Primary nav menu
        case 'rbc_navi_menu':
            //Get main menu
            //$main_menu = menu_navigation_links('main-menu');
            $main_menu = array();
            $year = rbc_year();
            foreach ($GLOBALS['departments'][$year] as $clean_name => $name) {
                $main_menu[$clean_name] = array(
                    'href' => rbc_year_prefix($year) . $clean_name,
                    'title' => $name,
                );
            }
            //Prints in the default theme for primary menu
            $block['content'] = theme('links__system_main_menu', array(
                'links' => $main_menu,
                'attributes' => array(
                    'id' => 'main-menu-links',
                    'class' => array('links', 'clearfix'),
                ),
                'heading' => array(
                    'text' => t('Main menu'),
                    'level' => 'h2',
                    'class' => array('element-invisible'),
                ),
            ));
            break;

        //My Computers Switcher
        case 'rbc_computers':
            if (!user_access('download private files')) return;
            $build = '';
            $computer = Computer::load();
            if (!$computer) {
                $block['subject'] = 'Identifying...';
                $build .= '<div id="identity_stat">Searching for plugin. Make sure the plugin is running.</div>';
                drupal_add_js('identity_check();', 'inline');
                drupal_add_js('triggerLink("' . rbc_plugin_identity_request() . '");', 'inline');
            } else {
                $block['subject'] = $computer->name;
                if ($computer->plugin_update_available()) {
                    if ($computer->version == 0) {
                        $build .= '<span id="block-setup-notice"><em>Please <a href="/computers/'.$computer->id.'">complete setup</a> to get started.</em></span><br /><br />';
                    } else {
                        $installer_fid = variable_get('rbc_plugin_installer', NULL);
                        if ($installer_fid && ($installer = file_load($installer_fid))) {
                            $build .= '<span>There is an update available for your plugin.<br />To continue please install the plugin:<br /><a href="' . file_create_url($installer->uri) . '">Download Latest Version</a></span>';
                        } else {
                            $build .= '<span>There is an update available for your plugin but the installer is not available yet. Please be patient.</span>';
                        }
                    }
                } else {
                    $build .= '<span id="filecount">'.$computer->file_count() . '</span> files<br />';

                    //Updates Section
                    $active = rbc_load_plugin_active(array('computer_id' => $computer->id));
                    $state = '';
                    if ($active) {
                        if ($active->status == -1) {
                            $state = 'no_path';
                        } else if ($active->token) {
                            $state = 'downloading';
                        }
                    }
                    $updates = $computer->update_count();
                    $build .= '<div id="plugin-update-container" ';
                    $build .= 'update_count="' . $updates . '" ';
                    $build .= 'state="' . $state . '" ';
                    if (!$computer->auto_update || $computer->paused) {
                        //Not Auto
                        $build .= 'update="plugin" ';
                    } else {
                        //Auto and good to go
                        $build .= 'update="auto" ';
                    }
                    $build .= '>';

                    //Update Available
                    if ($updates) {
                        $build .= '<span class="title warning">' . $updates . ' updates</span>';
                        //Update Running
                        if ($state == 'downloading') {
                            $pct = $active->total > 0 ? 100 * $active->status / $active->total : 0;
                            $build .= '<div class="progress"><div class="bar"><div class="filled" style="width:' . $pct . '%"></div><div class="text">' . round($pct) . '%</div></div></div>';
                            //Path Error
                        } else if ($state == 'no_path') {
                            $build .= '<div class="progress"><span class="error">Error: Path does not exist.</span>';
                            $build .= '<br /><a href="/computers/' . $computer->id . '">Update path</a> to dismiss.</div>';
                            //Not Running
                        } else if (!$state) {
                            $build .= '<div class="progress"><div class="bar">Update</div></div>';
                        }
                        //No Updates
                    } else {
                        $build .= '<span class="title ok">Up To Date</span>';
                    }
                    $build .= '</div>';
                }
            }
            // Edit Links
            $build .= '<div id="computer-edit-links">';
            // Add
            $build .= '<a id="computer-add" title="Add or Remove Computers" href="/computers"></a>';
            // Refresh
            $build .= '<a id="computer-switch" title="Identify Computer" class="disabled"></a>';
            // Settings
            $build .= '<a id="computer-settings" title="Computer Setup" ';
            $build .= (isset($_SESSION['computer_id'])) ? 'href="/computers/' . $_SESSION['computer_id'] . '"' : 'class="disabled" ';
            $build .= '></a>';
            $build .= '</div>';


            // Return Content
            $block['content'] = $build;
            break;

        /*         * **********************************************************************************************
         *  Year Switcher
         * ********************************************************************************************** */
        case 'rbc_year_switcher':
            if (!user_access('access past years')) return;
            $build = '';

            //Current
            //Switch
            $build .= '<div id="year-switch">';
            $build .= 'Year ' . rbc_year();
            $build .= '<a id="year-switch" title="Switch Year" onClick="jQuery(\'#switch-year-links\').slideToggle();"></a>';
            $build .= '</div>';

            //Switch Links
            $build .= '<ul id="switch-year-links">';
            $path = current_path();
            if (preg_match('/^\d{4}\/(.*)/', $path, $match)) $path = $match[1];
            foreach ($GLOBALS['years'] as $year) {
                $build .= '<a href="/' . rbc_year_prefix($year) . $path . '"><li>' . $year . '</li></a>';
            }
            $build .= '</ul>';

            // Return Content
            $block['content'] = $build;
            break;

        case 'rbc_action_buttons':
            $build = '';
            if (isset($GLOBALS['action_buttons'])) {
                $build .= '<div id="action-buttons"><ul>';
                foreach ($GLOBALS['action_buttons'] as $button) {
                    $build .= '<li>';
                    $build .= (isset($button['href'])) ? '<a' : '<span';
                    foreach (array_keys($button) as $id) {
                        if (substr($id, 0, 1) == '#') continue;
                        $build .= ' ' . $id . '="' . $button[$id] . '"';
                    }
                    $build .= '>' . $button['#text'];
                    $build .= (isset($button['href'])) ? '</a>' : '</span>';
                    $build .= '</li>';
                }
                $build .= '</ul></div>';
            }
            // Return Content
            $block['subject'] = '';
            $block['content'] = $build;
            break;
        case 'rbc_part_management':
            if (!user_access('commit')) return;
            $build = '<ul id="manage-parts">';
            $build .= '<a href="/' . rbc_year_prefix() . 'add"><li>Add</li></a>';
            $build .= '<a href="/' . rbc_year_prefix() . 'commit"><li>Commit</li></a>';
            $build .= '<a href="/' . rbc_year_prefix() . 'legacycommit"><li>Legacy Commit</li></a>';
            $build .= '</ul>';
            // Return Content
            $block['subject'] = 'Part Management';
            $block['content'] = $build;
            break;
        case 'rbc_reserved_parts':
            if (!user_access('commit')) return;
            $rows = db_select('rbc_reservation', 'r')
                    ->fields('r', array('part_id', 'year'))
                    ->condition('uid', $user->uid)
                    ->condition('unreserved', '', 'IS NULL')
                    ->execute()
                    ->fetchAll();

            if (!$rows) return;
            $build = '<ul id="reserved-parts">';
            foreach ($rows as $row) {
                $build .= rbc_reserveblock_link($row->part_id, $row->year);
            }
            $build .= '</ul>';
            // Return Content
            $block['subject'] = 'Reserved';
            $block['content'] = $build;
            break;
        case 'rbc_breadcrumb':
            if (!isset($GLOBALS['rbc_breadcrumb'])) return;

            $block['content'] = '<ul class="breadcrumb">';
            $path = '/' . rbc_year_prefix();
            $block['content'] .= '<li><a href="' . $path . 'updates">' . rbc_year() . '</a></li>';
            foreach ($GLOBALS['rbc_breadcrumb'] as $key => $name) {
                $path .= $key . '/';
                $block['content'] .= '<li><a href="' . $path . '">' . $name . '</a></li>';
            }
            $block['content'] .= '<li></li>';
            $block['content'] .= '</ul>';
            break;
    }
    return $block;
}
