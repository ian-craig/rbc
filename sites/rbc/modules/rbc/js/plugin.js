/* 
 * Copyright (C) 2014 Ian Craig
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// JavaScript Document
var stat_interval;
var checkUpdates_interval;
var timeout;
var dev = false;
var noReload = false;

jQuery(document).ready(function() {
    //Dev Functions
    //You can set var dev = true; in rbc_init() (rbc.module) for a given user to show dev without pissing people off.
    if (dev) {
        jQuery('#plugin-update-container').append('<pre>' + jQuery('#plugin-update-container').attr('key') + '</pre>');
    }
	
    if (jQuery('#plugin-update-container').attr('state') === 'downloading') {
        //Track Progress
        trackProgress();
    } else if (jQuery('#plugin-update-container').attr("update_count") > 0 && jQuery('#plugin-update-container').attr("update") === 'auto') {
        initUpdate();
    } else {
        //Check for updates
        checkUpdates_interval = setInterval('checkUpdates()', 15000); //Check every 15 seconds for testing
    }
    jQuery('#plugin-update-container .progress .bar').click(initUpdate);

    //Ajax Links
    jQuery('a.ajax_link').click(function(event) {
        event.preventDefault();
        jQuery.ajax({url: jQuery(this).attr('href')});
    });
    
    //Action Buttons
    jQuery('#action-button-reserve').click(function() {
        var part_id = jQuery(this).attr("part_id");
        if (part_id && jQuery(this).text() === 'Reserve') {
            ajaxAction('reserve', part_id, 'part');
        } else if (part_id && jQuery(this).text() === 'Unreserve') {
            ajaxAction('unreserve', part_id, 'part');
        }
    });
    jQuery('#action-button-force-update').click(function() {
        ajaxAction('force-update', jQuery(this).attr("part_id"), 'part');
    });
    //Reserved block x links
    state_xreserve_links();
    
    //Left Click Parts Table
    jQuery("tr.part_table_tr").click(function(event) {
        if(event.ctrlKey) {
            jQuery(this).toggleClass('selected');
        } else if (event.target.nodeName === "TD" || event.target.nodeName === "SPAN") {
            var $target = jQuery(event.target);
            window.location.href = jQuery(event.target).parents("tr.part_table_tr").attr("href");
        }
    });
    
    //Right Click Script
    jQuery('tr.part_table_tr').bind('contextmenu',function(e){
        jQuery(e.currentTarget).addClass('selected');
        //Set overlay to block other elements
        jQuery('<div class="vmenuOverlay"></div>').css({
            left : '0px', 
            top : '0px',
            position: 'absolute', 
            width: '100%', 
            height: jQuery('#page-wrapper').height()+'px', 
            zIndex: '100'
        })
        .click(function() {
            jQuery(this).remove();
            jQuery('#vmenu').hide();
            if (jQuery(".part_table_tr.selected").size() === 1) {
                jQuery(".part_table_tr.selected").removeClass('selected');
            }
        }).bind('contextmenu' , function(){
            return false;
        }).appendTo(document.body);
        //Position drop down
        var container = jQuery('#rightClickContainer').offset();
        jQuery('#vmenu').css({
            left: e.pageX - container.left, 
            top: e.pageY - container.top, 
            zIndex: '101'
        }).show();
        //Update drop down to suit selection
        //Hide All
        jQuery('#vmenu .element-reserve').hide();
        jQuery('#vmenu .element-unreserve').hide();
        jQuery('#vmenu .element-force-update').hide();
        //Show relevant ones
        jQuery(".part_table_tr.selected").each(function() {
            if (jQuery(this).attr('reserved') === "1") {
                jQuery('#vmenu .element-unreserve').show();
            } else {
                jQuery('#vmenu .element-reserve').show();
            }
            if (jQuery(this).attr('up-to-date') === 1) {
                jQuery('#vmenu .element-force-update').show();
            }
        });
        if (jQuery(".part_table_tr.selected").size() === 1) {
            jQuery('#vmenu .element-open-tab').show();
            jQuery('#vmenu .sep').show();
            jQuery('#vmenu .element-edit').show();
        } else {
            jQuery('#vmenu .element-open-tab').hide();
            jQuery('#vmenu .sep').hide();
            jQuery('#vmenu .element-edit').hide();
        }
        return false;
    });
    jQuery('#vmenu .element').live('click',function() {
        var button = jQuery(this);
        var list = [];
        jQuery(".part_table_tr.selected").each(function() {
            if (button.hasClass('element-reserve-open')) {
                list.push(jQuery(this).attr('filename'));
                if (jQuery(this).attr('reserved') !== "1") {
                    ajaxAction('reserve', jQuery(this).attr('part_id'), 'table');
                }
            } else if (button.hasClass('element-open')) {
                list.push(jQuery(this).attr('filename'));
            } else if (button.hasClass('element-open-tab')) {
                window.open(jQuery(this).attr('href'), '_blank');
            } else if (button.hasClass('element-reserve')) {
                if (jQuery(this).attr('reserved') !== "1") {
                    ajaxAction('reserve', jQuery(this).attr('part_id'), 'table');
                }
            } else if (button.hasClass('element-unreserve')) {
                if (jQuery(this).attr('reserved') === "1") {
                    ajaxAction('unreserve', jQuery(this).attr('part_id'), 'table');
                }
            } else if (button.hasClass('element-roll-forward')) {
                list.push(jQuery(this).attr('part_id'));
            } else if (button.hasClass('element-edit')) {
                window.location = jQuery(this).attr('href') + '/edit';
            } else if (button.hasClass('element-force-update')) {
                ajaxAction('force-update', jQuery(this).attr('part_id'), 'table');
            }
        });
        if (button.hasClass('element-roll-forward')) {
            window.location = '/' + jQuery('.part_table_container').attr('year') + '/rollforward/?part_ids=' + list.join(','); 
        } else if (button.hasClass('element-open') || button.hasClass('element-reserve-open')) {
            if (jQuery('.part_table_container').attr('open_protocol')) {
                triggerLink(jQuery('.part_table_container').attr('open_protocol') + '&file=' + list.join('&file='));
            }
        }
        
        jQuery('#vmenu').hide();
        jQuery('.vmenuOverlay').remove();
        jQuery(".part_table_tr.selected").removeClass('selected');
    });
    
    //File Upload - detect name
    jQuery('#edit-part-upload').change(function() {
        jQuery('#partname').html('');
        jQuery('.form-item-part_id').show();
        var filename = jQuery(this).val().split(/\\|\//).pop();
        jQuery.ajax({
            url:"/" + jQuery('body').attr('year') + "/legacycommit/ajax?filename=" + filename,
            success:function(partname){
                if (partname) {
                    jQuery('#partname').html(partname);
                    jQuery('.form-item-part_id').hide();
                }
            }
        });
    });
    
});

function checkUpdates() {
    jQuery.ajax({
        url:"/plugin/js/count",
        success:function(count){
            var previous = jQuery('#plugin-update-container').attr("update_count");
            if (count > previous) {
                //Update Title and attributes
                jQuery('#plugin-update-container').attr("update_count", count);
                jQuery('#plugin-update-container span.title').text(count + ' updates').removeClass('ok').addClass('warning');
                //Set up download button if it's not there already
                if (previous === 0) {
                    //RBC Updater Update Button
                    var update = jQuery('#plugin-update-container').attr("update");
                    if (update === 'auto' || update === 'plugin') {
                        jQuery('#plugin-update-container').append('<div class="progress"><div class="bar">Update</div></div>');
                        //Trigger Auto Updates or let user trigger
                        if (update === 'auto') {
                            initUpdate();
                        } else {
                            jQuery('#plugin-update-container .progress .bar').click(initUpdate);
                        }
                    //Manual Update Button
                    } else {
                        jQuery('#plugin-update-container').append('<br /><a style="cursor:pointer;" onclick="manualUpdate();">Download Update</a>');
                    }
                }
            } else if (count < previous && !noReload) {
                location.reload(true);
            }
        }
    });
}

function initUpdate() {
    jQuery.ajax({
        url:"/plugin/js/update",
        success:function(src){
            //Start first download
            triggerLink(src);
            jQuery('#plugin-update-container').attr('state', 'downloading');
            jQuery('div#plugin-update-container .progress .bar').html('Downloading...');
            //Stop checking for updates
            clearInterval(checkUpdates_interval);
            //Track Progress
            trackProgress();
        }
    });
}

function triggerLink(src) {
    jQuery.ajax({url:src,timeout:1000});
    /*
    jQuery("#main-wrapper iframe").remove();
    iframe = document.createElement("iframe");
    jQuery(iframe).attr("style", "width:0px;height:0px;border:none;visibility:hidden;").attr("src", src);
    jQuery('#main-wrapper').append(iframe);
    */
}

function trackProgress() {
    var state = jQuery('#plugin-update-container').attr('state');
    //Stop checking for updates
    if (checkUpdates_interval) {
        clearInterval(checkUpdates_interval);
    }
    //Take action on current state
    if (state === 'downloading') {
        //Check status every 
        stat_interval = setInterval("updateProgress()",1000);
    }
}

function updateProgress() {
    //Get Current Status
    jQuery.ajax({
        url:"/plugin/js/stat",
        dataType: "json",
        success: function(update){
            if (update.status >= 0) {
                var percentage = 100 * update.status / update.total;
                if (percentage > 100) {
                    percentage = 100;
                }
                //jQuery('span#filecount').text(update.filecount);
                jQuery('div#plugin-update-container .progress .bar').html('<div class="filled" style="width:' + percentage + '%"></div><div class="text">' + Math.round(percentage) + '%</div>');
            }
            if (!update.active) {
                clearInterval(stat_interval);
                if (update.status > 0) {
                    //Update complete
                    if (noReload) {
                        jQuery('#plugin-update-container .notice').remove();
                        jQuery('#plugin-update-container').append('<span class="notice">Update Complete.<br />Waiting for you to finish this form before continuing...</span>');
                    } else {
                        location.reload(true);
                    }
                } else if (update.status === -1) {
                    //Path Error
                    jQuery('#plugin-update-container div.progress').html('<span class="error">Error: Path does not exist.</span>');
                } else if (update.status === -2) {
                    //Connection Error
                    jQuery('#plugin-update-container div.progress').html('<span class="error">Error: Connection Timed Out.</span>');
                } else  {
                    //Unknown Error
                    jQuery('#plugin-update-container div.progress').html('<span class="error">Error: Unknown Failure. Please try again.</span>');
                }
            }
        }
    });
}

function ajaxAction(action, part_id, page, year) {
    if(typeof(year)==='undefined') year = jQuery('body').attr('year');
    if (page === 'part' && jQuery('#action-button-reserve').attr("part_id") === part_id) {
        if (action === 'reserve') {
            jQuery('#action-button-reserve').text('Reserving...');
        } else if (action === 'unreserve') {
            jQuery('#action-button-reserve').text('Unreserving...');
        }
    }
    jQuery.ajax({
        url:"/part/ajax?action=" + action + "&part_id=" + part_id + "&year=" + year,
        success:function(data){
            var stat = eval(data);
            var data = stat[0];
            if (data.result === 'success') {
                if (action === 'reserve') {
                    jQuery('ul#reserved-parts').append(data.block_string);
                    state_xreserve_links();
                    if (page === 'part') {
                        jQuery('#action-button-reserve').text('Unreserve').addClass('green');
                        jQuery('div#reservation-notice').html('<span>This items has been reserved</span>');
                    } else if (page === 'table') {
                        jQuery('#part_table_tr_' + part_id).attr('reserved', 1);
                        jQuery('#part_table_tr_' + part_id).find("span.reserved").html('Reserved');
                    }
                } else if (action === 'unreserve') {
                    jQuery('ul#reserved-parts .part-' + part_id).remove();
                    if (page === 'part' && jQuery('#action-button-reserve').attr("part_id") === part_id) {
                        jQuery('#action-button-reserve').text('Reserve').removeClass('green');
                        jQuery('div#reservation-notice').html('');
                    } else if (page === 'table') {
                        jQuery('#part_table_tr_' + part_id).attr('reserved', 0);
                        jQuery('#part_table_tr_' + part_id).find("span.reserved").html('');
                    }
                } else if (action === 'force-update') {
                    checkUpdates();
                    if (page === 'part') {
                        jQuery('#action-button-force-update').fadeOut();
                    } else if (page === 'table') {
                        jQuery('#part_table_tr_' + part_id).attr('up-to-date', 0);
                    }
                }
            } else {
                if (!data.error) data.error = 'Unknown error.';
                jQuery('div#content').prepend('<div class="messages error"><h2 class="element-invisible">Error message</h2>' + data.error + '</div>');
            }
        }
    });
}

function state_xreserve_links() {
    jQuery('#reserved-parts .unreserve-x').click(function() {
        var page = 'table';
        if (jQuery('tr.part_table_tr').length === 0) {
            page = 'part';
        }
        ajaxAction('unreserve', jQuery(this).attr('part_id'), page, jQuery(this).attr('year'));
    });
}

function checkPlugin() {
    jQuery('#setup-errors').html();
    jQuery('#setup-check #throbber').addClass('active');
    //Stop checking for updates
    clearInterval(checkUpdates_interval);
    //Check For version - stage 1
    jQuery.ajax({
        url:"/plugin/js/setup_request",
        success:function(src){
            //Start Version check
            triggerLink(src);
            //Track Progress
            stat_interval = setInterval('checkPluginStat()', 1000);
        },
        error:function() {
            jQuery('#setup-check').prepend('<div class="messages error">Could not start check, please try again.</div>');
            jQuery('#setup-check #throbber').removeClass('active');
        }
    });
}

function checkPluginStat() {
    //Get Current Status
    jQuery.ajax({
        url:"/plugin/js/setup_stat",
        dataType: "json",
        success: function(update){
            if (!update.active) {
                clearInterval(stat_interval);
                jQuery('#setup-check #throbber').removeClass('active');
                
                if (update.status > 0 || update.status === -1) {
                    if (update.version === 0) {
                        //Version Error
                        jQuery('#setup-errors').html('<div class="messages error">Your plugin is out of date. Please install the one above and try again.</div>');
                    } else {
                        if (update.status === -1) {
                            //Path Error
                            jQuery('#setup-errors').html('<div class="messages error">Error: Path does not exist.</div>');
                        } else {
                            //Success
                            jQuery('#setup-check').remove();
                            jQuery('#setup-complete').show();
                            window.location.replace(document.location + "?connected=1");
                        }
                    }
                } else if (update.status === -2) {
                    //Connection Error
                    jQuery('#setup-errors').html('<div class="messages error">Error: Time out.</div>');
                } else  {
                    //Unknown Error
                    jQuery('#setup-errors').html('<div class="messages error">An unknown error has occurred, please try again.</div>');
                }
            }
        }
    });
}

function tableSortTweaks() {
    jQuery(".table-select-processed tbody tr").css("cursor", "pointer").each(function () {
        if (jQuery(this).find(":checkbox").attr("checked")) {
            jQuery(this).addClass("selected");
        }
    });
    jQuery(".table-select-processed tbody tr").click(function(event) {
        var target = jQuery(event.target);
        if(!target.is(":checkbox")) {
            if (jQuery(this).find(":checkbox").attr("checked")) {
                jQuery(this).removeClass("selected");
                jQuery(this).find(":checkbox").attr("checked", false);
            } else {
                jQuery(this).addClass("selected");
                jQuery(this).find(":checkbox").attr("checked", true);
            }
        }
    });
}

function identity_stat() {
    jQuery.ajax({
        dataType: "json",
        url:'/plugin/js/identity_stat',
        success:function(result) {
            if (result.status > 0) {
                clearInterval(stat_interval);
                jQuery("#block-rbc-rbc-computers h2").text(result.name);
                if (result.status === 1) {
                    jQuery('#identity_stat').text('Found computer... Reloading.');
                    location.reload(true);
                } else {
                    jQuery('#identity_stat').html('This computer has not been set up.<br /><a href="/computers">Add or Setup</a> a computer.');
                }
            } else if (result.status < 0) {
                 clearInterval(stat_interval);
                if (result.status === -2) {
                    jQuery("#block-rbc-rbc-computers h2").text('Timeout');
                    jQuery('#identity_stat').html('Check your plugin is running, or <br /><a href="/computers">Add a computer</a>');
                } else {
                    jQuery("#block-rbc-rbc-computers h2").text('Error');
                    jQuery('#identity_stat').text('An unknown error occurred. Please try again.');
                }
            }
        }
    });
}

function identity_check() {
    clearInterval(checkUpdates_interval);
    stat_interval = setInterval('identity_stat()', 1000);
    
}
