<?php

/* 
 * Copyright (C) 2014 Ian Craig
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// $Id$
// Page Functions for Redback CAD module
// Created by Ian Craig - minicraig@gmail.com


/* * **********************************************************************************************
 * Home Page / Updates Page
 * ********************************************************************************************** */

function rbc_pages_updates_graph() {
    $start_time = strtotime('midnight')-7*24*3600;
    $versions = db_select('rbc_version', 'v')
            ->fields('v', array('id', 'uid', 'date'))
            ->condition('date', $start_time, '>')
            ->execute()
            ->fetchAll();
    $all = array();
    $byuser = array();
    $byusercount = array();
    foreach($versions as $version) {
        $all[$version->date] = !array_key_exists($version->date, $all) ? 1 : $all[$version->date]+1;
        if (!array_key_exists($version->uid, $byuser)) $byuser[$version->uid] = array();
        $byuser[$version->uid][$version->date] = !array_key_exists($version->date, $byuser[$version->uid]) ? 1 : $byuser[$version->uid][$version->date]+1;
        $byusercount[$version->uid] = !array_key_exists($version->uid, $byusercount) ? 1 : $byusercount[$version->uid]+1;
    }
    arsort($byusercount);
    $topusers = array_slice(array_keys($byusercount), 0, 3);
    
    $groups['Total'] = $all;
    foreach($topusers as $uid) {
        $user = user_load($uid);
        $groups[$user->name] = $byuser[$uid];
    }
    
    $build = '';

    $js = "google.load('visualization', '1', {'packages':['annotatedtimeline']});";
    $js .= 'google.setOnLoadCallback(drawChart2);';
    $js .= "\n\nfunction drawChart2() {var data = new google.visualization.DataTable();
data.addColumn('date', 'Time');\n";

    //Declare cols and set up an empty template of a day to work off
    $empty_day = array();
    foreach (array_keys($groups) as $username) {
        $js .= "data.addColumn('number', '$username');\n";
        $empty_day[$username] = 0;
    }

    $history = array();

    //Declare all days from first to today as zero
    for ($i = $start_time; $i <= time(); $i += 24*3600) {
        $javaDateString = date('Y, m-1, d', $i);
        $history[$javaDateString] = $empty_day;
    }
//Fill in history
    foreach ($groups as $username => $uploads) {
        foreach ($uploads as $date => $count) {
            $javaDateString = date('Y, m-1, d', $date);
            $history[$javaDateString][$username] += $count;
        }
    }

    foreach ($history as $javaDateString => $values) {
        $js .= "data.addRow([new Date($javaDateString), " . implode(', ', $values) . "]);\n";
    }
    $js .= 'var options = {width: 600, height: 200, title: "Upload History", displayAnnotations: false, displayZoomButtons: false, displayRangeSelector: false};

var chart = new google.visualization.AnnotatedTimeLine(document.getElementById("chart_div2"));
chart.draw(data, options);
}';

    drupal_add_js('https://www.google.com/jsapi');
    drupal_add_js($js, 'inline');

    $build .= '<div id="chart_div2" style="width: 690px; height: 250px;"></div>';
    $build .= '<style type="text/css">#chart_div2 table, #chart_div2 td, #chart_div2 tr, #chart_div2 tbody {padding:0;background:none;border:none;margin:0;}</style>';
    return $build;
}

function rbc_pages_updates() {
    if (!user_access('download private files')) return rbc_pages_welcome_anonymous();
    drupal_set_title('Recent Updates');
    $build = '';

    $build .= rbc_pages_updates_graph();
    
    //Create a list of headers for your Html table (see Drupal 7 docs for theme_table here
    $header = array(
        array('data' => 'Part'),
        array('data' => 'Name'),
        //array('data' => 'Department'),
        array('data' => 'Updated'),
        array('data' => 'Comment'),
    );

    $select = db_select('rbc_version', 'v')
            ->extend('PagerDefault')  //Pager Extender
            ->limit(20);
    $select->join('rbc_part', 'p', 'p.id=v.part_id');
    $select->join('rbc_part_instance', 'i', 'i.part_id=v.part_id AND i.year=v.year');
    $select->join('rbc_commit', 'c', 'c.id=v.commit');
    $select->addField('p', 'name');
    $select->addField('p', 'is_assembly');
    $select->addField('v', 'date');
    $select->addField('v', 'uid');
    $select->addField('c', 'message');
    $select->addField('v', 'part_id');
    $select->addField('v', 'version');
    $select->condition('v.year', rbc_year());
    $select->condition('i.removed', 0);
    $select->orderBy('v.date', 'DESC');
    $results = $select->execute();
    $rows = array();
    foreach ($results as $version) {
        $part = Part::load($version->part_id);
        $shortname = (drupal_strlen($part->name) > 32) ? drupal_substr($part->name, 0, 29) . '...' : $part->name;
        $rows[] = array(
            'data' => array(
                array('data' => l($part->part_id_str, $part->path())),
                array(
                    'data' => l($shortname, $part->path()),
                    'width' => '180px',
                ),
                array('data' => format_date($version->date, 'custom', 'j M') . ' by ' . ul($version->uid),
                    'width' => '100px',
                ),
                array('data' => format_shorten_string($version->message, 54)),
            ),
        );
    }
    //Theme the html table
    $build .= theme('table', array(
        'header' => $header,
        'rows' => $rows,
        'sticky' => TRUE, //Optional to indicate whether the table headers should be sticky
        'empty' => 'No updates found...', //Optional empty text for the table if resultset is empty
            )
    );

    //Append pager
    $build .= theme('pager', array(
        'tags' => array()
            )
    );

    return $build;
}

function rbc_pages_welcome_anonymous() {
    $build = '';
    drupal_set_title('Welcome to Redback CAD');

    $build .= '<p>This site has been developed to manage the CAD database for <a href="http://redbackracing63.com/">UNSW Redback Racing</a>.</p>';
    $build .= '<p>The site is strictly for members of the team.</p>';
    $build .= '<p>If you\'d like to find out more about Redback Racing head over to our main website: <a href="http://redbackracing63.com/">http://redbackracing63.com/</a>';

    return $build;
}

/* * **********************************************************************************************
 * Department / All Part List Pages
 * ********************************************************************************************** */

function rbc_pages_parts_table($department, $category_machine_name = NULL) {
    $build = '';
    global $rbc_breadcrumb;
    $rbc_breadcrumb = array($department => $GLOBALS['departments'][rbc_year()][$department]);
    //Category page title
    if ($category_machine_name == 'nocategory') $category_machine_name = NULL;
    if ($category_machine_name) {
        $category = db_select('rbc_category', 'c')
                ->fields('c')
                ->condition('machine_name', $category_machine_name)
                ->condition('department', $department)
                ->condition('year', rbc_year())
                ->execute()
                ->fetchObject();
        if (!$category) return MENU_NOT_FOUND;
        drupal_set_title($GLOBALS['departments'][rbc_year()][$department] . ' - ' . $category->name);
        //Breadcrumb
        $rbc_breadcrumb[$category->machine_name] = $category->name;
    } else {
        drupal_set_title($GLOBALS['departments'][rbc_year()][$department]);
    }

    //////////Categories//////////
    if ($department != 'all' && !$category_machine_name) {
        $cats = db_select('rbc_category', 'c')
                ->fields('c', array('id', 'name', 'machine_name'))
                ->orderBy('name', 'ASC')
                ->condition('department', $department)
                ->condition('year', rbc_year())
                ->execute()
                ->fetchAll();
        $build .= '<div id="cat-tile-container">';
        foreach ($cats as $cat) {
            $select = db_select('rbc_part_instance', 'i');
            $select->join('rbc_part', 'p', 'p.id=i.part_id');
            $select->addField('i', 'image_fid');
            $select->condition('i.department', $department);
            $select->condition('i.category', $cat->id);
            $select->condition('i.year', rbc_year());
            $select->condition('i.image_fid', '', 'IS NOT NULL');
            $select->orderBy('p.is_assembly', 'DESC');
            $select->orderBy('i.removed', 'ASC');
            $img_fid = $select->execute()->fetchCol();
            //Insert Tile Block
            $build .= '<a href="/' . rbc_year_prefix() . $department . '/' . $cat->machine_name . '"><div class="cat-tile-block"><div class="images">';
            //For some reason the range function returns nothing sometimes so we'll use a counter
            $i = 0;
            foreach ($img_fid as $fid) {
                $part_image = file_load($fid);
                $build .= theme_image_style(array(
                    'style_name' => 'cat_tile_thumb',
                    'path' => rbc_public_uri($part_image->uri),
                    'height' => NULL,
                    'width' => NULL,
                        ));
                if (++$i > 3) break;
            }
            $build .= '</div><div class="title">' . $cat->name . '</div></div></a>';
        }
        $build .= '</div>';
    } else {
        $GLOBALS['action_buttons'][] = array(
            '#text' => 'Delete Category',
            'href' => '/' . $department . '/' . $category->machine_name . '/delete',
            'title' => 'Remove this category and make all it\'s parts uncategorised.',
        );
    }

    // Parts Table
    $condition = db_and();
    $condition->condition('i.department', $department);
    if (isset($category)) $condition->condition('i.category', $category->id);
    $build .= rbc_parts_table($condition, false);
    
    return $build;
}

function rbc_parts_table($condition, $show_department = true) {
    global $user;
    $build = '';
    
    // Filtering removed parts
    if (isset($_GET['view'])) {
        if ($_GET['view'] == 'current') {
            $_SESSION['view_removed_parts'] = false;
        } else if ($_GET['view'] == 'all') {
            $_SESSION['view_removed_parts'] = true;
        }
    }
    $view_removed = (isset($_SESSION['view_removed_parts']) && $_SESSION['view_removed_parts']) ? true : false;
    $open_protocol = rbc_plugin_uri('open', array('year' => rbc_year()));
    
    $build .= '<div class="part_table_container" year="' . rbc_year() . '" open_protocol="' . $open_protocol . '">';
    $build .= '<span class="table-filters">';
    $build .= ($view_removed) ? '<a href="?view=current">Hide Removed Parts</a>' : '<a href="?view=all">Show Removed Parts</a>';
    $build .= '</span>';

    $build .= '<div id="rightClickContainer">
     <div id="vmenu">
        <div class="element element-open">Open</div>
        <div class="element element-reserve-open">Reserve & Open</div>
        <div class="element element-open-tab">Open Page in new tab</div>
        <div class="sep"></div>
        <div class="element element-reserve">Reserve</div>
        <div class="element element-unreserve">Unreserve</div>
        <div class="element element-roll-forward">Roll Forward</div>
        <div class="element element-edit">Edit</div>
        <div class="element element-force-update">Force Update</div>
     </div>';

    $header = array();
    $header[] = array('data' => 'Open');
    $header[] = array('data' => 'Part Info', 'field' => 'name', 'sort' => 'asc');
    if ($view_removed) $header[] = array('data' => 'Status', 'field' => 'status', 'width' => 60);
    if ($show_department) $header[] = array('data' => 'Department', 'field' => 'department');
    $header[] = array('data' => 'Updated', 'width' => 90);

    $query = db_select('rbc_part', 'p');
    $query->join('rbc_part_instance', 'i', 'i.part_id=p.id');
    $query->extend('PagerDefault')->limit(30);
    $query->extend('TableSort')->orderByHeader($header);//Field to sort on is picked from $header
    $query->condition('i.year', rbc_year());
    $query->condition($condition);
    if (!$view_removed) $query->condition('i.removed', 0);
    $query->addField('p', 'id');
    $query->addField('p', 'name');
    $query->addField('i', 'department');
    $query->addField('p', 'is_assembly');
    $query->addField('i', 'removed');
    $query->addField('i', 'image_fid');
    $results = $query->execute();

    $rows = array();

    foreach ($results as $row) {
        $part = Part::load($row->id);
        $data = array();
        $image = theme_image_style(array(
            'style_name' => 'part_table_thumb',
            'path' => rbc_public_uri($part->image()->uri),
            'height' => NULL,
            'width' => NULL,
                ));
        // Thumbnail
        $assem_or_part = $part->is_assembly ? 'assembly' : 'part';
        $type_icon = '<img src="/' . drupal_get_path('module', 'rbc') . '/img/' . $assem_or_part . '_icon_20.png" />';
        $data[] = '<a class="part_table_thumb ajax_link" href="' . rbc_plugin_uri('open', array('year' => rbc_year(), 'file' => $part->file()->filename)) . '">' . $image . $type_icon . '</a>';

        // Title etc
        $tr = '';
        $tr .= '<span class="part-status">';
        $tr .= ($part->reserved() == $user->uid) ? '<span class="reserved">Reserved</span>' : '<span class="reserved"></span>';
        $tr .= '</span>';
        $tr .= '<span class="title-part_id">' . $part->name . '</br>' . $part->part_id_str . '</span>';
        $data[] = $tr;

        // Status
        if ($view_removed) $data[] = $part->status();

        // Department
        if ($show_department) $data[] = l($GLOBALS['departments'][rbc_year()][$part->department], $part->department);
        
        // Date & Author
        $data[] = format_date($part->date, 'custom', 'D jS M') . '<br /> by ' . ul($part->uid);

        // Attributes
        $class = array();
        $class[] = 'part_table_tr';
        $file = $part->file();
        $rows[] = array(
            'data' => $data,
            'class' => $class,
            'id' => 'part_table_tr_' . $part->id,
            'href' => '/' . $part->path(),
            'filename' => $file ? $file->filename : '',
            'part_id' => $part->id,
            'reserved' => ($part->reserved() == $user->uid),
            'up-to-date' => (rbc_download_date($part->id) >= $part->date) ? 1 : 0,
        );
    }
    $build .= '</div>'; //rightClickContainer
    //Theme the html table
    $build .= theme('table', array(
        'header' => $header,
        'rows' => $rows,
        'sticky' => TRUE,
        'empty' => 'No parts found.',
    ));

    //Append pager
    $build .= theme('pager', array('tags' => array()));
    return $build;
}

/* * **********************************************************************************************
 * Part Pages
 * ********************************************************************************************** */

function rbc_pages_part($part_id) {
    global $user;
    $computer = Computer::load();
    $part = Part::load($part_id);
    $build = '';
    
    if (!$part) {
        if (($years = Part::years($part_id))) {
            drupal_set_title('Part not found in ' . rbc_year());
            $build .= 'This part only exists in ';
            for ($i = 0; $i < count($years); $i++) {
                $part = Part::load($part_id, $years[$i]);
                $build .= $part ? l($years[$i], $part->path()) : $years[$i];
                if ($i == count($years)-2) $build .= ' and ';
                if ($i < count($years)-2) $build .= ', ';
            }
            return $build;
        } else {
            drupal_set_title('Part not found');
            return 'Sorry, I got nothin\'';
        }
    }
        
    //Check the category name is correct
    if (current_path() != $part->path()) drupal_goto($part->path());
    
    $GLOBALS['rbc_breadcrumb'][$part->department] = $GLOBALS['departments'][rbc_year()][$part->department];
    if ($part->category()->id) $GLOBALS['rbc_breadcrumb'][$part->category()->machine_name] = $part->category()->name;
    $GLOBALS['rbc_breadcrumb'][$part->part_id_str] = $part->name;

    $build .= '<div id="part-info" class="clearfix">';
    //Image
    $build .= '<a href="' . file_create_url($part->image()->uri) . '" target="_blank"><div id="part-image">';
    $build .= theme_image_style(array(
        'style_name' => 'medium',
        'path' => rbc_public_uri($part->image()->uri),
        'height' => NULL,
        'width' => NULL,
        'attributes' => array('style' => array('float:left;')),
            ));
    $build .= '</div></a>';
    //Reserved Warning
    $build .= '<div id="reservation-notice">';
    if ($part->reserved()) {
        $build .= '<span>This part has been reserved by ' . ul($part->reserved()) . '</span>';
    }
    $build .= '</div>';
    //Version Number
    $build .= '<h1 id="part_id">' . $part->part_id_str . ' <span id="version">version ' . $part->version . '</span> ';
    // Removed notice
    if ($part->removed) {
        $build .= '<span id="status" class="removed">Removed.</span>';
    }
    $build .= '</h1>';
    
    //Part Name
    $build .= '<h1 id="name">' . $part->name . '</h1>';
    //Description
    $build .= check_markup(html_entity_decode($part->description), 'filtered_html') . '<br />';
    $build .= '</div>'; //end part-info
    
    //Relations
    $build .= '<div id="relation-info">';
    $parents = rbc_relation_parents($part->id);
    if ($parents) {
        $build .= '<p>In Assemblies: ';
        foreach ($parents as $parent) {
            $build .= pl($parent);
            if ($parent != end($parents)) $build .= ', ';
        }
        $build .= '</p>';
    }
    $children = rbc_relation_children($part->id);
    if ($children) {
        $build .= '<p>Parts: ';
        foreach ($children as $child) {
            $build .= pl($child);
            if ($child != end($children)) $build .= ', ';
        }
        $build .= '</p>';
    }
    $build .= '</div>'; //end relation-info
    //Action Buttons
    //Open
    if (!rbc_download_date($part->id)) {
        $l_class = 'disabled';
        $l_title = 'Never Downloaded';
    } else if (rbc_download_date($part->id) < $part->date) {
        $l_class = '';
        $l_title = 'Update Available';
    } else {
        $l_class = '';
        $l_title = 'Click to open part.';
    }
    $l_class .= ' ajax_link';
    $GLOBALS['action_buttons'][] = array(
        '#text' => 'Open',
        'href' => rbc_plugin_uri('open', array( 
                        'year' => rbc_year(),
                        'file' => $part->file()->filename,
                    )),
        'class' => $l_class,
        'title' => $l_title,
    );
    //Force Update
    if ($computer && $computer->download_date($part->id)) {
        $GLOBALS['action_buttons'][] = array(
            '#text' => 'Force Update',
            'title' => 'Force re-download if part in your next update.',
            'id' => 'action-button-force-update',
            'part_id' => $part->id,
        );
    }
    if (user_access('commit')) {
        //Reserve
        if ($part->reserved() && $part->reserved() != $user->uid) {
            $GLOBALS['action_buttons'][] = array(
                '#text' => 'Reserve',
                'class' => 'disabled',
                'title' => 'This part has been reserved by ' . ul($part->reserved()),
            );
        } else if ($part->reserved()) {
            $GLOBALS['action_buttons'][] = array(
                '#text' => 'Unreserve',
                'part_id' => $part->id,
                'class' => 'green',
                'id' => 'action-button-reserve',
            );
        } else {
            $GLOBALS['action_buttons'][] = array(
                '#text' => 'Reserve',
                'part_id' => $part->id,
                'id' => 'action-button-reserve',
            );
        }
        //Edit
        $GLOBALS['action_buttons'][] = array(
            '#text' => 'Edit',
            'title' => 'Edit part description and details.',
            'href' => request_uri() . '/edit',
        );
        //Delete
        if ($user->uid == $part->uid && $part->version == 0 && $part->date > (time() - 5 * 60)) {
            $GLOBALS['action_buttons'][] = array(
                '#text' => 'Delete',
                'title' => 'Delete the part if it was uploaded by mistake.',
                'href' => '/' . rbc_year_prefix() . 'delete/' . $part->part_id_str,
            );
        }
    }

    //Version Tables
    $years = db_select('rbc_part_instance', 'i')
            ->fields('i', array('year'))
            ->condition('part_id', $part->id)
            ->orderBy('year', 'DESC')
            ->execute()
            ->fetchCol();
    
    unset($years[array_search(rbc_year(), $years)]);
    array_unshift($years, rbc_year());
    foreach ($years as $year) {
        $select = db_select('rbc_version', 'v');
        $select->join('rbc_commit', 'c', 'c.id=v.commit');
        $select->addField('v', 'version');
        $select->addField('v', 'date');
        $select->addField('c', 'message');
        $select->addField('v', 'commit');
        $select->addField('v', 'uid');
        $select->addField('v', 'drawing');
        $select->addField('v', 'file_id');
        $select->condition('part_id', $part->id);
        $select->condition('year', $year);
        $select->orderBy('v.version', 'DESC');
        if ($year != rbc_year()) $select->range(0, 3);
        $versions = $select->execute();
        $header = array(
            array('data' => 'V#', 'width' => 16, 'title' => 'Version Number'),
            array('data' => 'Date', 'width' => 65),
            array('data' => 'Commit Message'),
            array('data' => 'User', 'width' => 60),
            array('data' => 'Drawing', 'width' => 60),
        );
        $rows = array();
        foreach ($versions as $version) {
            if ($version->file_id) {
                $file = file_load($version->file_id);
                if (!$file) $version->file_id = NULL;
            }
            if ($version->drawing) {
                $drawing = file_load($version->drawing);
                if (!$drawing) $version->drawing = NULL;
            }
            $commit = Commit::load($version->commit);
            $rows[] = array(
                'data' => array(
                    ($version->file_id) ? l($version->version, file_create_url($file->uri)) : $version->version,
                    format_date($version->date, 'custom', 'd/m/Y'),
                    $commit->teaser(),
                    ul($version->uid),
                    ($version->drawing) ? l('Download', file_create_url($drawing->uri)) : '',
                ),
            );
        }
        if ($version->version > 0 && $year != rbc_year()) {
            $rows[] = array(
                'data' => array(
                    array(
                        'data' => '<a href="/'.$part->path($year).'" class="morelink" title="View '.$year.'">...</a>',
                        'colspan' => 5,
                    )
                ),
            );
        }

        $build .= '<div class="version-table">';
        //Theme the html table
        $build .= theme('table', array(
            'header' => $header,
            'rows' => $rows,
            'caption' => '<h2>' . $year . ' Version History</h2>', //Optional Caption for the table
            'empty' => 'No versions found...', //Optional empty text for the table if resultset is empty
                )
        );
        $build .= '</div>'; //end version-table
        
        if ($year == rbc_year() && count($years) > 1) {
            $build .= '<br />This part appears in other years:<hr>';
        }
    }
    return $build;
}

function rbc_pages_part_ajax() {
    if (isset($_GET['action']) && isset($_GET['part_id'])) {
        global $user;
        $computer = Computer::load();
        if (!$computer) return;
        $year = $_GET['year'];
        if (!in_array($year, $GLOBALS['years'])) return;
        //Reserve Part
        if ($_GET['action'] == 'reserve') {
            if (!user_access('commit')) {
                echo '[{ 
                    "result": "error",
                    "error": "You do not have permission to reserve a file.",
                    }]';
                return;
            }
            if (($reserved_by = Part::reserved_by($_GET['part_id'],$year))) {
                echo '[{ 
                    "result": "error",
                    "error": "Part is already reserved by ' . ul($reserved_by) . '",
                    }]';
                return;
            }
            $id = db_insert('rbc_reservation')
                    ->fields(array(
                        'part_id' => $_GET['part_id'],
                        'uid' => $user->uid,
                        'date' => time(),
                        'year' => $year,
                    ))
                    ->execute();
            if ($id) {
                echo '[{ 
                    "result": "success",
                    "block_string": ' . json_encode(rbc_reserveblock_link($_GET['part_id']), $year) . ',
                    }]';
            } else {
                echo '[{ 
                    "result": "error",
                    "error": "Failed to send request to database.",
                    }]';
            }
            //UnReserve Part
        } else if ($_GET['action'] == 'unreserve') {
            $id = db_update('rbc_reservation')
                    ->fields(array('unreserved' => time()))
                    ->condition('part_id', $_GET['part_id'])
                    ->condition('year', $year)
                    ->condition('uid', $user->uid)
                    ->execute();
            if ($id) {
                echo '[{ 
                    "result": "success",
                    }]';
            } else {
                echo '[{ 
                    "result": "error",
                    "error": "Failed to send request to database.",
                    }]';
            }
            //Force Update
        } else if ($_GET['action'] == 'force-update') {
            $deleted = db_delete('rbc_download')
                    ->condition('computer_id', $computer->id)
                    ->condition('part_id', $_GET['part_id'])
                    ->condition('year', $year)
                    ->execute();
            if ($deleted) {
                echo '[{ 
                    "result": "success",
                    }]';
            } else {
                echo '[{ 
                    "result": "error",
                    "error": "Failed to send request to database.",
                    }]';
            }
        }
    }
}

/**
 * Display details of a commit.
 * Includes author, date, message and a table of parts included 
 * @param type $commit_id
 * @return string
 */
function rbc_pages_commit($commit_id) {
    $commit = Commit::load($commit_id);
    if (!$commit) return MENU_NOT_FOUND;
    drupal_set_title('Commit #' . $commit->id);
    
    $build = '';
    $build .= '<div style="width:100%;text-align:right;">' . format_date($commit->date, 'medium') . ' by ' . ul($commit->uid) . '</div>';
    $build .= check_markup($commit->message, 'filtered_html');

    // Parts Table
    $part_ids = db_select('rbc_version', 'v')
            ->fields('v', array('part_id'))
            ->condition('commit', $commit->id)
            ->execute()
            ->fetchCol();
    if (count($part_ids)) {
        $condition = db_and();
        $condition->condition('p.id', $part_ids, 'IN');
        $build .= rbc_parts_table($condition);
    } else {
        $build .= "Oh noes! There don't seem to be any parts in this commit.";
    }
    return $build;
}
