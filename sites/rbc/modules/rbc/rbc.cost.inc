<?php

/* 
 * Copyright (C) 2014 Ian Craig
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

function rbc_cost_data() {
    if (!isset($_GET['key']) || !isset($_GET['pid'])) return MENU_NOT_FOUND;
    $correct_key = variable_get('rbc_cost_key', NULL);
    if (!$correct_key || $_GET['key'] != $correct_key) return MENU_NOT_FOUND;
    $part = Part::load($_GET['pid']);
    if (!$part || !$part->instance_id) return MENU_NOT_FOUND;

    $data = array('','','');

    // Drawing Name
    if ($part->drawing && $drawing = file_load($part->drawing)) {
        $data[0] = $drawing->filename;
    }
    
    // Part URL
    $data[1] = url($part->path(), array('absolute' => TRUE));

    // Image Filepath
    $data[2] = filesystem_path($part->image()->uri);

    print implode(';',$data);
}

function rbc_cost_img() {
    if (!isset($_GET['key']) || !isset($_GET['pid'])) return MENU_NOT_FOUND;
    $correct_key = variable_get('rbc_cost_key', NULL);
    if (!$correct_key || $_GET['key'] != $correct_key) return MENU_NOT_FOUND;
    $part = Part::load($_GET['pid']);
    if (!$part || !$part->instance_id) return MENU_NOT_FOUND;
    
    // open the file in a binary mode
    $filepath = filesystem_path($part->image()->uri);
    $fp = fopen($filepath, 'rb');
    // send the right headers
    header("Content-Type: image/png");
    header("Content-Length: " . filesize($filepath));
    // dump the picture and stop the script
    fpassthru($fp);
    exit;
}