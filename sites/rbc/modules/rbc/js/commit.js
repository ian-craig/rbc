/* 
 * Copyright (C) 2014 Ian Craig
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

var check_interval;
var uploading = 0;
var queue = new Array();
var delete_list = new Array();
var commit_list = new Object();
var year;

(function ($, Drupal, window, document, undefined) {

jQuery(document).ready(function() {
    year = $('body').attr('year');
    
    //Watch Manual Upload Input
    $('input#manual-part_id').keyup(function() {
        if ($(this).val().length === 4) {
            var part_id = $(this).val();
            $('#manual-list #throbber').addClass('active');
            $.ajax({
                url:"/" + year + "/commit/ajax?part_id=" + part_id,
                success:function(block){
                    if (block) {
                        $('input#manual-part_id').val('');
                        $('#manual-list #throbber').removeClass('active');
                        $('#manual-list .add-list').html(block);
                        if (!$('#manual-list .uploadblock').hasClass('disabled')) {
                            $('#manual-list .uploadblock').click(transferEnqueue);
                        }
                    }
                },
                complete:function() {
                    $('#manual-list #throbber').removeClass('active');
                }
            });
        } else {
            $('#manual-list .add-list').html('');
        }
    });

    //Enqueue suggested
    $('#suggest-list .uploadblock').click(transferEnqueue);
    
    //Sumbit Trigger
    $('form#rbc-forms-commit').submit(submit_form);
    
    setInterval('queueManager();', 100);
});

})(jQuery, Drupal, this, this.document);

function updateDev() {
    jQuery("#dev .queue").html(queue.toString());
    jQuery("#dev .uploading").html(uploading);
    jQuery("#dev .commit_list").html(JSON.stringify(commit_list));
    //jQuery("#dev .check").html(check_interval);
}

function submit_form(event) {
    // Message box error - better to do this via javascript before submit kills out interface
    if (jQuery('#edit-message').val() === '') {
        jQuery('#edit-message').addClass('error');
        event.preventDefault();
        return;
    } else {
        jQuery('#edit-message').removeClass('error');
    }
    // Check if we are still waiting
    if (!uploading && !queue.length) {
        jQuery('input[name="commit_list"]').val(JSON.stringify(commit_list));
        jQuery('input[name="delete_list"]').val(JSON.stringify(delete_list));
    } else {
        if (jQuery('input.form-submit').hasClass('waiting')) {
            jQuery('input.form-submit').removeClass('waiting');
            jQuery('#form-submit-waiting').text('');
        } else {
            jQuery('input.form-submit').addClass('waiting');
            jQuery('#form-submit-waiting').text('Waiting for files to upload...');
        }
        event.preventDefault();
    }
}

function queueManager() {
    if (!uploading && queue.length) {
        var part_id = queue.shift();
        requestUpload(part_id, year);
        updateState(part_id, "uploading", "Uploading");
    } else if (!uploading && !queue.length && jQuery('input.form-submit').hasClass('waiting')) {
        jQuery('input.form-submit').removeClass('waiting');
        jQuery('#form-submit-waiting').text('');
        jQuery('form#rbc-forms-commit').submit();
    }
    updateDev();
}

function transferEnqueue(event) {
    // Save the object
    var $target = jQuery(event.target).parents('.uploadblock');
    // Read Part ID
    var part_id = $target.attr('part_id');
    // Remove all instances of this Part ID from lists
    jQuery('.add-list .uploadblock[part_id='+part_id+']').remove();
    // Try and add it to the queue
    if (jQuery.inArray(part_id, queue) === -1 && part_id !== uploading && !(part_id in commit_list)) {
        jQuery('#commit-list .hint').remove();
        $target.unbind('click').appendTo(jQuery('#commit-list'));
        updateState(part_id, 'waiting', "Waiting");
        $target.children('.close-box').click(cancelUpload);
        queue.push(part_id);
    }
}

function cancelUpload(event) {
    var $target = jQuery(event.target).parents('.uploadblock');
    var part_id = $target.attr('part_id');
    $target.remove();
    if (part_id === uploading) {
        clearInterval(check_interval);
        uploading = 0;
    } else if (part_id in commit_list) {
        delete commit_list[part_id];
    } else if (jQuery.inArray(part_id, queue) !== -1) {
        queue.splice(jQuery.inArray(part_id, queue),1);
    }
}

function updateState(part_id, state, text) {
    jQuery('#commit-list .uploadblock[part_id=' + part_id + '] .state').removeClass('waiting').removeClass('uploading').removeClass('done').removeClass('error').addClass(state);
    jQuery('#commit-list .uploadblock[part_id=' + part_id + '] .state .text').html(text);
    if (state === 'error') {
        jQuery('#commit-list .uploadblock[part_id=' + part_id + ']').click(transferEnqueue);
        jQuery('input.form-submit').removeClass('waiting');
    }
}

function requestUpload(part_id) {
    uploading = part_id;
    updateState(part_id, "uploading");
    //Request upload url and trigger upload
    jQuery.ajax({
        url:"/plugin/js/upload_request?part_id=" + part_id + '&year=' + year,
        dataType: "json",
        success:function(data) {
            //Start Version check
            jQuery.ajax({url: data.url});
            //Track Progress
            check_interval = setInterval('checkUpload();', 1000);
        }
    });
}

function checkUpload() {
    //Get Current Status
    jQuery.ajax({
        url:"/plugin/js/upload_stat",
        dataType: "json",
        data: {'part_id': uploading},
        success: function(upload){
            if (upload.part_id !== uploading) return;
            if (upload.status > 1) {
                uploading = 0;
                clearInterval(check_interval);
                updateState(upload.part_id, "done", "Done");
                commit_list[upload.part_id] = upload.part_fid;
            } else if (upload.status < 0) {
                uploading = 0;
                clearInterval(check_interval);
                if (upload.status === -2) {
                    updateState(upload.part_id, "error", "Timed Out");
                } else if (upload.status === -3) {
                    updateState(upload.part_id, "error", "File Not Found");
                } else {
                    updateState(upload.part_id, "error", "Error");
                }
            }
        }
    });
}