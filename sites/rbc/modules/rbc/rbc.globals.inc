<?php

/* 
 * Copyright (C) 2014 Ian Craig
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// $Id$
// Constants for Redback CAD module
// Created by Ian Craig - minicraig@gmail.com

//The current / default 'year'
$GLOBALS['current_year'] = 2014;

//Defines departments available throughout the site and database
//Format: 'simple_name' => 'Full Department Name',
//simple_name max 30 characters.
//WARNING - CHAINGING THIS ARRAY MAY FUCK UP THE department REFERENCES IN THE DATABASE IF THERE ARE ALREADY PARTS IN THE DATABASE.
$GLOBALS['departments'] = array(
    '2012' => array(
        'car' => 'RB12',
        'aero' => 'Aero/Bodywork', 
        'brakes' => 'Brakes', 
        'front-chassis' => 'Front Chassis', 
        'rear-chassis' => 'Rear Chassis',
        'electrics' => 'Electrics', 
        'powertrain' => 'Powertrain', 
        'suspension' => 'Suspension',
        'universal' => 'Universal',
    ),
    '2013' => array(
        'car' => 'RB13',
        'aero' => 'Bodywork', 
        'brakes' => 'Brakes', 
        'front-chassis' => 'Front Chassis', 
        'rear-chassis' => 'Rear Chassis',
        'electrics' => 'Electrics', 
        'driveline' => 'Driveline',
        'powertrain' => 'Powertrain', 
        'suspension' => 'Suspension',
        'universal' => 'Universal',
    ),
    '2014' => array(
        'car' => 'RB14',
        'aero' => 'Bodywork', 
        'brakes' => 'Brakes', 
        'front-chassis' => 'Front Chassis', 
        'rear-chassis' => 'Rear Chassis',
        'electrics' => 'Electrics', 
        'driveline' => 'Driveline',
        'powertrain' => 'Powertrain', 
        'suspension' => 'Suspension',
        'universal' => 'Universal',
    ),
);

//Plugin
$GLOBALS['domain'] = 'rbc.redbackracing63.com';
$GLOBALS['plugin_version'] = 8;


// DO NOT CHANGE ANYTHING AFTER HERE
$GLOBALS['years'] = array_keys($GLOBALS['departments']);
define("REQ_STAR", ' <span class="form-required" title="This field is required.">*</span>');