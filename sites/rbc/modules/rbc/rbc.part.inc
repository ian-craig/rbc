<?php

/* 
 * Copyright (C) 2014 Ian Craig
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

class Part {
    //Part
    public $id, $part_id_str, $name, $is_assembly;
    //Part Instance
    public $instance_id, $description, $removed, $image_fid, $department, $category_id, $year;
    //Current Version
    public $version_id, $version, $drawing, $date, $part_fid, $uid;
    private $reserved_by = -1;
    
    public static function load($part_id, $year = NULL) {
        $new = new Part();
        //Setup
        if ($year == NULL) $year = rbc_year();
        $new->year = $year;
        $new->id = $part_id;
        $new->part_id_str = format_part_id($new->id);
        //Part
        $part = db_select('rbc_part', 'p')
            ->fields('p')
            ->condition('id', $part_id)
            ->execute()
            ->fetchObject();
        if (!$part) return NULL;
        $new->name = $part->name;
        $new->is_assembly = $part->is_assembly;
        //Part Instance
        $pi = db_select('rbc_part_instance', 'i')
                ->fields('i')
                ->condition('part_id', $part_id)
                ->condition('year', $new->year)
                ->execute()
                ->fetchObject();
        if (!$pi) return NULL;
        $new->instance_id = $pi->id;
        $new->description = $pi->description;
        $new->removed = $pi->removed;
        $new->image_fid = $pi->image_fid;
        $new->department = $pi->department;
        $new->category_id = $pi->category;
        //Version
        $v = db_select('rbc_version', 'v')
                ->fields('v')
                ->condition('part_id', $part_id)
                ->condition('year', $new->year)
                ->orderBy('version', 'DESC')
                ->execute()
                ->fetchObject();
        if ($v) {
            $new->version_id = $v->id;
            $new->version = $v->version;
            $new->drawing = $v->drawing;
            $new->date = $v->date;
            $new->part_fid = $v->file_id;
            $new->uid = $v->uid;
        } else {
            $new->version_id = $new->version = $new->drawing = $new->date = NULL;
        }
        return $new;
    }
    
    public function path($year = NULL) {
        if (!$year) $year = $this->year;
        return rbc_year_prefix($year) . $this->department . '/' . $this->category()->machine_name . '/' . $this->part_id_str;
    }
    
    public function status() {
        return $this->removed ? "Removed" : "Active";
    }
    
    public function reserved() {
        if ($this->reserved_by == -1)
            $this->reserved_by = Part::reserved_by($this->id, $this->year);
        return $this->reserved_by;
    }
    
    public function category() {
        if ($this->category_id) {
            $category = db_select('rbc_category', 'c')
                    ->fields('c')
                    ->condition('id', $this->category_id)
                    ->execute()
                    ->fetchObject();
        }
        if (!$this->category_id || !$category) {
            //Build the empty / nocategory category
            $category = new stdClass();
            $category->id = 0;
            $category->name = null;
            $category->department = $this->department;
            $category->year = $this->year;
            $category->machine_name = 'nocategory';
        }
        return $category;
    }
    
    public function image() {
        if ($this->image_fid && $file = file_load($this->image_fid)) {
            return $file;
        } else if (($file = file_load(variable_get('rbc_nopartimage', NULL)))) {
            return $file;
        }
    }
    
    public function file() {
        return file_load($this->part_fid);
    }

    public static function create($name, $is_assembly) {
        $part_id = db_insert('rbc_part')
                ->fields(array(
                    'name' => $name,
                    'is_assembly' => $is_assembly,
                ))
                ->execute();
        return Part::load($part_id);
    }
    
    public function commit($fid, $commit_id) {
        global $user;
        // Get new version number
        $this->version = (isset($this->version)) ? $this->version+1 : 0;
        // Save the file
        $file = file_load($fid);
        $ext = pathinfo($file->filename, PATHINFO_EXTENSION);
        $uri = 'private://rbc/parts/' . $this->part_id_str . '-' . $this->year . '-' . $this->version . '.' . $ext;
        if ($file->uri != $uri) {
            if (!file_usage_list($file)) {
                $file = file_move($file, $uri, FILE_EXISTS_ERROR);
            } else {
                $file = file_copy($file, $uri, FILE_EXISTS_ERROR);
            }
            if (!$file) {
                drupal_set_message('Error: Could not move part file into repository.', 'error');
                return;
            }
        }
        $file->status = FILE_STATUS_PERMANENT;
        $file->filename = $this->part_id_str . '-' . $this->year . '.' . $ext;
        // Exception for 2012 when no year was included in filename
        if ($this->year == 2012) $file->filename = $this->part_id_str . '.' . $ext;
        file_save($file);
        $this->part_fid = $file->fid;
        // Save version info
        $this->version_id = db_insert('rbc_version')
                ->fields(array(
                    'part_id' => $this->id,
                    'version' => $this->version,
                    'year' => $this->year,
                    'file_id' => $this->part_fid,
                    'date' => time(),
                    'uid' => $user->uid,
                    'commit' => $commit_id,
                ))
                ->execute();

        file_usage_add($file, 'rbc', 'part', $this->version_id);
        //You MUST add a usage else next time the part is rolled forward etc it will move this
        //file instead of copying it because it won't realise it's in use
        
        // Update image
        $image = $this->generate_part_image($file->uri);
        if (!$image) drupal_set_message('Failed to generate part image.', 'error');
    }
    
    public function save() {
        // Part
        $fields = array(
            'id' => $this->id,
            'name' => $this->name,
            'is_assembly' => $this->is_assembly ? 1 : 0,
        );
        $this->id = rbc_updateorcreate('rbc_part', $fields);
        $this->part_id_str = format_part_id($this->id);
        
        // Part Instance
        $pi_fields = array(
            'id' => $this->instance_id,
            'part_id' => $this->id,
            'description' => $this->description,
            'removed' => $this->removed ? 1 : 0,
            'image_fid' => $this->image_fid,
            'department' => $this->department,
            'category' => $this->category_id,
            'year' => $this->year,
        );
        $this->instance_id = rbc_updateorcreate('rbc_part_instance', $pi_fields);
        
        // Drawing
        if ($this->version_id) {
            $old_drawing = db_select('rbc_version', 'v')
                    ->fields('v', array('drawing'))
                    ->condition('id', $this->version_id)
                    ->execute()
                    ->fetchField();
            if ($this->drawing != $old_drawing) {
                if ($old_drawing) {
                    //Delete Old
                    $file = file_load($old_drawing);
                    file_usage_delete($file, 'rbc', 'part_drawing', $old_drawing->id);
                    file_delete($file);
                }
                if ($this->drawing) {
                    //Save New
                    $file = file_load($this->drawing);
                    $ext = pathinfo($file->uri, PATHINFO_EXTENSION);
                    $file->filename = $this->part_id_str . '-' . $this->year . '-' . $this->version . '.' . $ext;
                    if (!file_usage_list($file)) {
                        $file = file_move($file, 'private://rbc/drawings/' . $file->filename, FILE_EXISTS_REPLACE);
                    } else {
                         $file = file_copy($file, 'private://rbc/drawings/' . $file->filename, FILE_EXISTS_REPLACE);
                    }
                    $file->status = FILE_STATUS_PERMANENT;
                    file_save($file);
                    file_usage_add($file, 'rbc', 'part_drawing', $this->version_id);
                    $this->drawing = $file->fid; //Because file_copy changes the fid
                }
                db_update('rbc_version')
                        ->fields(array('drawing' => $this->drawing))
                        ->condition('id', $this->version_id)
                        ->execute();
            }
        }
    }
    
    public function delete() {
        //Delete Part Download Records
        db_delete('rbc_download')
                ->condition('part_id', $this->id)
                ->condition('year', $this->year)
                ->execute();
        //Delete Part Relations
        db_delete('rbc_relation')
                ->condition(db_or()
                        ->condition('parent_part_id', $this->id)
                        ->condition('child_part_id', $this->id)
                        ->condition('year', $this->year)
                )
                ->execute();
        //Delete Part Reservations
        db_delete('rbc_reservation')
                ->condition('part_id', $this->id)
                ->condition('year', $this->year)
                ->execute();
        //Delete Part Files, Drawings
        $rows = db_select('rbc_version', 'v')
                ->fields('v', array('id','file_id', 'drawing'))
                ->condition('part_id', $this->id)
                ->condition('year', $this->year)
                ->condition('file_id', '', 'IS NOT NULL')
                ->execute()
                ->fetchAll();
        foreach ($rows as $row) {
            foreach (array('file_id', 'drawing') as $field) {
                if (is_numeric($row->$field) && $file = file_load($row->$field)) {
                    $suffix = ($field == 'file_id') ? '' : '_' . $field;
                    file_usage_delete($file, 'rbc', 'part' . $suffix, $row->id);
                    file_delete($file);
                }
            }
        }
        //Delete Versions
        db_delete('rbc_version')
                ->condition('part_id', $this->id)
                ->condition('year', $this->year)
                ->execute();
        //Delete Part Image
        if (($image = $this->image())) {
            file_usage_delete($image, 'rbc', 'part_image', $this->id);
            file_delete($image);
        }
        //Delete Part Instance
        db_delete('rbc_part_instance')
                ->condition('id', $this->instance_id)
                ->execute();
        //Delete Part if we need to
        $other_year = db_select('rbc_part_instance', 'i')
                ->fields('i', array('id'))
                ->condition('part_id', $this->id)
                ->execute()
                ->fetchField();
        if (!$other_year) {
            db_delete('rbc_part')
                    ->condition('id', $this->id)
                    ->execute();
        }
    }
    
    /**
     * Delete the most recent version of this part.
     */
    public function deleteVersion() {
        if ($this->version == 0) {
            $this->delete();
            return;
        }
        //Delete Part Download Records
        db_delete('rbc_download')
                ->condition('part_id', $this->id)
                ->condition('year', $this->year)
                ->condition('date', $this->date, '>=')
                ->execute();
        //Delete Part Relations
        db_delete('rbc_relation')
                ->condition(db_or()
                        ->condition('parent_part_id', $this->id)
                        ->condition('child_part_id', $this->id)
                        ->condition('year', $this->year)
                )
                ->execute();
        //Refresh Part Relations
        
        //Delete Part Files, Drawings
        foreach (array('part_fid', 'drawing') as $field) {
            if (is_numeric($this->$field) && $file = file_load($this->$field)) {
                $suffix = ($field == 'part_fid') ? '' : '_' . $field;
                file_usage_delete($file, 'rbc', 'part' . $suffix, $this->version_id);
                file_delete($file);
            }
        }
        //Delete Versions
        db_delete('rbc_version')
                ->condition('id', $this->version_id)
                ->execute();
        //Refresh Part Image
        $this->generate_part_image();
    }
    
    public function generate_part_image($uri = NULL) {
        if ($uri == NULL) {
            if (!$this->part_fid) return;
            $file = file_load($this->part_fid);
            if (!isset($file->uri)) return;
            $uri = $file->uri;
        }
        $filepath = filesystem_path($uri);
        $shards = preg_split('/[-\.]/', basename($filepath));
        $image_filename = $shards[0] . '-' . $shards[1] . '.png';
        system("gsf cat $filepath PreviewPNG > /tmp/$image_filename");
        $image = file_save_data(file_get_contents("/tmp/$image_filename"), 'private://rbc/part_images/' . $image_filename, FILE_EXISTS_REPLACE);
        file_usage_add($image, 'rbc', 'part_image', $this->instance_id);
        unlink("/tmp/$image_filename");
        if (getimagesize(filesystem_path($image->uri))) {
            //Delete old file
            if ($this->image_fid && $old = file_load($this->image_fid)) {
                file_usage_delete($old, 'rbc', 'part_image', $this->instance_id);
                file_delete($old);
            }
            #Save the part image
            $this->image_fid = $image->fid;
            #Because we need an instance_id to set usage the instance must have already been created
            db_update('rbc_part_instance')
                ->fields(array('image_fid' => $this->image_fid))
                ->condition('part_id', $this->id)
                ->condition('year', $this->year)
                ->execute();
            
            #Clear Cached thumbnails
			rbc_clear_image_cache($image);
            
            return $image;
        } else {
            //Cancel
            file_usage_delete($image, 'rbc', 'part_image', $this->instance_id);
            file_delete($image);
            return;
        }
    }
    
    public function findRelations() {
        if (!$this->part_fid) return;
        $file = file_load($this->part_fid);
        if (!isset($file->uri)) return;
        $uri = $file->uri;
        if (pathinfo($file->uri, PATHINFO_EXTENSION) != 'SLDASM') return;

        $filepath = filesystem_path($uri);
        system("gsf cat $filepath Header2 > /tmp/$this->part_id_str-Header2");
        $contents = file_get_contents("/tmp/$this->part_id_str-Header2");
        unlink("/tmp/$this->part_id_str-Header2");

        dev_print($contents);
        
        $matches = array();
        preg_match_all('/([\w\:\.\/\\\\].)+(\d.\d.\d.\d).\..S.L.D.\w.\w.\w/', $contents, $matches);
        
        $relations = array();
        foreach($matches[2] as $part_id) {
            $part_id = intval(preg_replace('/\D/', '', $part_id));
            if ($part_id == $this->id) continue;
            $relations[] = $part_id;
        }
        return $relations;
    }
    
    public function renderUploadBlock($state = '', $state_text = 'Available') {
        $disabled = false;
        // Are we allowed to upload it?
        global $user;
        //Double check parts are all in database
        if (!$this->version_id) {
            $state_text = 'Manual Required';
            $state = 'error';
            $disabled = true;
        //Check user is up to date on this part in current computer
        } else if ($this->date > rbc_download_date($this->id)) {
            $state_text = 'Out of date';
            $state = 'error';
            $disabled = true;
        }
        //Check part is not reserved
        if ($this->reserved() && $this->reserved() != $user->uid) {
            $state_text = 'Reserved';
            $state = 'error';
            $disabled = true;
        }
        // Render
        return theme('rbc_part_uploadblock', array(
            'part' => $this, 
            'state' => $state, 
            'state_text' => $state_text, 
            'disabled' => $disabled,
        ));
    }
    
    
    /* -------------------- STATIC Functions ---------------------------------*/
    
    public static function years($part_id) {
        $years = db_select('rbc_part_instance', 'i')
                ->fields('i', array('year'))
                ->condition('part_id', $part_id)
                ->execute()
                ->fetchCol();
        return $years;
    }
    
    public static function reserved_by($part_id, $year = NULL) {
        if ($year == NULL) $year = rbc_year();
        $uid = db_select('rbc_reservation', 'r')
                ->fields('r', array('uid'))
                ->condition('part_id', $part_id)
                ->condition('year', $year)
                ->condition('unreserved', '', 'IS NULL')
                ->execute()
                ->fetchField();
        return $uid;
    }
    
}
